﻿/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

INSERT INTO `etats` (`ETA_CODE`, `ETA_COMPTEUR`, `ETA_DESCRIPTION`, `ETA_LIBELLE`, `ETA_STATUT`, `ETA_WOR_ID`, `ETA_DELAI`, `ETA_SEQUENCE`) VALUES
	('attente_validation_montant_taxe', 0, 'En attente de validation du montant de la taxe', 'En attente de validation du montant de la taxe', 1, 1, NULL, NULL),
	('EN_ATTENTE_AVIS_DUA', 0, 'En attente avis DUA', 'En attente avis DUA', 1, 1, NULL, NULL),
	('EN_ATTENTE_PAIEMENT', 0, 'En attente de paiement', 'En attente de paiement', 1, 1, NULL, NULL),
	('PENDING_ARCHIVED', 0, 'En attente archivage', 'En attente archivage', 1, 1, NULL, 17),
	('REJET_DUA', 0, 'Dossier rejeté par le DUA', 'Dossier rejeté par le DUA', 1, 1, NULL, NULL),
	('SOUMIS_PAIEMENT', 0, 'Soumission pour paiement', 'Soumis pour paiement', 1, 1, NULL, NULL),
	('VALIDE_DUA', 0, 'Dossier validé par le DUA', 'Dossier validé par le DUA', 1, 1, NULL, NULL),
	('VERIFICATION_SITE', 0, 'Verification par agent.bd du bon site de depot', 'Vérification site', 1, 1, NULL, NULL);


	INSERT INTO `organisations` (`ORG_CODE`, `ORG_DESCRIPTION`, `ORG_TRANSVERSALE`, `ORG_LIBELLE`, `ORG_PAR_CODE`, `ORG_SIT_CODE`, `telepaiementState`, `ORG_STATUT`) VALUES
		('CABMB', 'Cabinet Mairie de Bambilor', 0, 'Cabinet Mairie de Bambilor', 'CABMS', 'CR. BAMBYLOR_CA', 'NS', 1),
		('CABMCS', 'Cabinet Mairie Commune de Sangalkam', 0, 'Cabinet Mairie Commune de Sangalkam', 'CABMS', 'COM. SANGALKAM_CA', 'NS', 1),
		('CABMD', 'Mairie Diamniadio', 0, 'Mairie Diamniadio', NULL, 'COM. DIAMNIADIO_CA', 'NS', 1),
		('CABMJ', 'Mairie Jaxaay Niacoul', 0, 'Mairie Jaxaay Niacoul', NULL, 'COM. JAXAAY PARCELLE NIAKOUL RAP_CA', 'NS', 1),
		('CABMJN', 'Cabinet Mairie Commune Jaxaay Niakoul RAB', 0, 'Cabinet Mairie Commune Jaxaay Niakoul RAB', 'CABMS', 'COM. JAXAAY PARCELLE NIAKOUL RAP_CA', 'NS', 1),
		('CABMRE', 'Mairie de Rufisque Est', 0, 'Mairie de Rufisque Est', 'CABMR', 'CA. RUFISQUE EST_CA', 'NS', 1),
		('CABMRN', 'Mairie Rufisque Nord', 0, 'Mairie Rufisque Nord', 'CABMR', 'CA. RUFISQUE CENTRE (NORD)_CA', 'NS', 1),
		('CABMRO', 'Mairie Rufisque Ouest', 0, 'Mairie Rufisque Ouest', 'CABMR', 'CA. RUFISQUE OUEST_CA', 'NS', 1),
		('CABMS', 'Mairie Sangalkam', 0, 'Mairie Sangalkam', NULL, 'COM. SANGALKAM_CA', 'NS', 1),
		('CABMSE', 'Mairie Sendou', 0, 'Mairie Sendou', NULL, 'COM. SENDOU_CA', 'NS', 1),
		('CABMSEB', 'Mairie Sebikotane', 0, 'Mairie Sebikotane', NULL, 'COM. SEBIKOTANE_CA', 'NS', 1),
		('CABMT', 'Mairie Tivaouane Peulh Niague', 0, 'Mairie Tivaouane Peulh Niague', NULL, 'CR. TIVAOUANE PEULH-NIAGHA_CA', 'NS', 1),
		('CABMTP', 'Cabinet Mairie Commune de Tivaoune Peulh', 0, 'Cabinet Mairie Commune de Tivaoune Peulh', 'CABMS', 'CR. TIVAOUANE PEULH-NIAGHA_CA', 'NS', 1),
		('CABMY', 'Cabinet Mairie Yenne', 0, 'Cabinet Mairie Yenne', 'CABPR', 'CR. YENE_CA', 'NS', 1),
		('MAIRIE_BAMBYLOR', 'Mairie de Bambylor', 0, 'Mairie de Bambylor', NULL, 'CR. BAMBYLOR_CA', NULL, 1),
		('MAIRIE_BARGNY', 'Mairie de Bargny', 0, 'Mairie de Bargny', NULL, 'COM. BARGNY_CA', 'S', 1),
		('MAIRIE_BISCUITERIE', 'Mairie biscuiterie', 0, 'Mairie biscuiterie', NULL, 'CA. BISCUITERIE_CA', NULL, 1),
		('MAIRIE_CAMBERENE', 'Mairie de camberene', 0, 'Mairie de camberene', NULL, 'CA. CAMBERENE_CA', NULL, 1),
		('MAIRIE_COLOBANE', 'Mairie de colobane', 0, 'Mairie de colobane', NULL, 'CA. COLOBANE/FASS/GUEULE TAPEE_CA', NULL, 1),
		('MAIRIE_DALIFORT', 'Maire de dalifort', 0, 'Maire de dalifort', NULL, 'CA. DALIFORD_CA', NULL, 1),
		('MAIRIE_DIACKSAO', 'Mairie de Diack Sao', 0, 'Mairie de Diack Sao', NULL, 'CA. DIACK SAO_CA', NULL, 1),
		('MAIRIE_DIAMEGUENE', 'Mairie de DIAMEGUENE', 0, 'Mairie de DIAMEGUENE', NULL, 'CA. DIAMAGUENE/SICAP MBAO _CA', NULL, 1),
		('MAIRIE_DIEUPPEUL', 'Mairie de Dieuppeul', 0, 'Mairie de Dieuppeul', NULL, 'CA. DIEUPPEUL DERKLE_CA', NULL, 1),
		('MAIRIE_FANN', 'Mairie de Fann', 0, 'Mairie de Fann', NULL, 'CA. FANN/POINT E/ AMITIE_CA', NULL, 1),
		('MAIRIE_GOLFSUD', 'Mairie de Golf Sud', 0, 'Mairie de Golf Sud', NULL, 'CA. GOLF SUD_CA', NULL, 1),
		('MAIRIE_GOREE', 'Mairie de Goree', 0, 'Mairie de Goree', NULL, 'CA. GOREE_CA', NULL, 1),
		('MAIRIE_GRANDAKAR', 'Mairie de grand dakar', 0, 'Mairie de grand dakar', NULL, 'CA. GRAND DAKAR_CA', NULL, 1),
		('MAIRIE_GRANDYOFF', 'Mairie de Grand Yoff', 0, 'Mairie de Grand Yoff', NULL, 'CA. GRAND YOFF_CA', NULL, 1),
		('MAIRIE_GUINORD', 'Mairie de Guinaw rail nord', 0, 'Mairie de Guinaw rail nord', NULL, 'CA. GUINAW RAIL NORD_CA', NULL, 1),
		('MAIRIE_GUISUD', 'Mairie de Guinaw rail sud', 0, 'Mairie de Guinaw rail sud', NULL, 'CA. GUINAW RAIL SUD_CA', NULL, 1),
		('MAIRIE_HANN', 'Mairie de Hann', 0, 'Mairie de Hann', NULL, 'CA. HANN/ BEL AIR _CA', NULL, 1),
		('MAIRIE_HLM', 'Mairie Hlm', 0, 'Mairie Hlm', NULL, 'CA. HLM_CA', NULL, 1),
		('MAIRIE_KEURMASSAR', 'Mairie Keur Massar', 0, 'Mairie Keur Massar', NULL, 'CA. KEUR MASSAR_CA', NULL, 1),
		('MAIRIE_LIMAMOULAYE', 'Mairie de ndiareme limamoulaye', 0, 'Mairie de ndiareme limamoulaye', NULL, 'CA. NDIAREME LIMAMOULAYE_CA', NULL, 1),
		('MAIRIE_MALIKA', 'Mairie de Malika', 0, 'Mairie de Malika', NULL, 'CA. MALIKA_CA', NULL, 1),
		('MAIRIE_MBAO', 'Mairie de Mbao', 0, 'Mairie de Mbao', NULL, 'CA. MBAO_CA', 'S', 1),
		('MAIRIE_MEDGOUNASS', 'Mairie de Médina Gounass', 0, 'Mairie de Médina Gounass', NULL, 'CA. MEDINA GOUNASS_CA', NULL, 1),
		('MAIRIE_MEDINA', 'Mairie de Médina', 0, 'Mairie de Médina', NULL, 'CA. MEDINA_CA', NULL, 1),
		('MAIRIE_MERMOZ', 'Mairie de Mermoz', 0, 'Mairie de Mermoz', NULL, 'CA.MERMOZ/ SACRE -COEUR_CA', NULL, 1),
		('MAIRIE_NGOR', 'Mairie de Ngor', 0, 'Mairie de Ngor', NULL, 'CA. NGOR_CA', 'S', 1),
		('MAIRIE_NOTAIRE', 'Mairie de notaire', 0, 'Mairie de notaire', NULL, 'CA. SAM NOTAIRE_CA', NULL, 1),
		('MAIRIE_OUAKAM', 'Mairie de Ouakam', 0, 'Mairie de Ouakam', NULL, 'CA. OUAKAM_CA', 'S', 1),
		('MAIRIE_PA', 'Mairie de Parcelles', 0, 'Mairie de Parcelles', NULL, 'CA. PARCELLES ASSAINIES_CA', NULL, 1),
		('MAIRIE_PATTEDOIE', 'Maire de Patte d\'Oie', 0, 'Maire de Patte d\'Oie', NULL, 'CA. PATTE DOIE_CA', NULL, 1),
		('MAIRIE_PIKEST', 'Mairie de Pikine EST', 0, 'Mairie de Pikine EST', NULL, 'CA. PIKINE EST_CA', NULL, 1),
	        ('MAIRIE_PIKOUEST', 'Mairie de Pikine Ouest', 0, 'Mairie de Pikine Ouest', NULL, 'CA. PIKINE OUEST_CA', NULL, 1),
	        ('MAIRIE_PIKNORD', 'Mairie de Pikine Nord', 0, 'Mairie de Pikine Nord', NULL, 'CA. PIKINE SUD/NORD_CA', NULL, 1),
		('MAIRIE_PLATEAU', 'Mairie de Plateau', 0, 'Mairie de Plateau', NULL, 'CA. PLATEAU_CA', NULL, 1),

		('MAIRIE_RUFISQUE_EST', 'Mairie Rufisque Est', 0, 'Mairie Rufisque Est', NULL, 'CA. RUFISQUE EST_CA', 'NS', 1),
		('MAIRIE_RUFISk_EST', 'Mairie Rufisque Est', 0, 'Mairie Rufisque Est', NULL, 'CA. RUFISQUE EST_CA', 'NS', 1),


		('MAIRIE_RUFISQUE_NORD', 'Mairie Rufisque Nord', 0, 'Mairie Rufisque Nord', NULL, 'CA. RUFISQUE CENTRE (NORD)_CA', 'NS', 1),
		('MAIRIE_RUFISQUE_OUEST', 'Mairie Rufisque Ouest', 0, 'Mairie Rufisque Ouest', NULL, 'CA. RUFISQUE OUEST_CA', 'NS', 1),
		('MAIRIE_SICAP', 'Mairie SICAP', 0, 'Mairie SICAP', NULL, 'CA. SICAP LIBERTE_CA', NULL, 1),
		('MAIRIE_THIAGARE', 'Mairie de Thiaroye gare', 0, 'Mairie de Thiaroye gare', NULL, 'CA. THIAROYE GARE_CA', NULL, 1),
		('MAIRIE_THIAMER', 'Mairie de Thiaroye sur Mer', 0, 'Mairie de Thiaroye sur Mer', NULL, 'CA. THIAROYE /MER_CA', NULL, 1),
		('MAIRIE_THIAROYEKAO', 'Mairie de Thiaroyekao', 0, 'Mairie de Thiaroyekao', NULL, 'CA. DJIDAH THIAROYE KAO_CA', NULL, 1),
		('MAIRIE_WAKHINANE', 'Mairie de Wakhinane', 0, 'Mairie de Wakhinane', NULL, 'CA. WAKHINANE NIMZATT_CA', NULL, 1),
		('MAIRIE_YENE', 'Mairie de Yene', 0, 'Mairie de Yene', NULL, 'CR. YENE_CA', 'S', 1),
		('MAIRIE_YEUMBEULSUD', 'Mairie de Yeumbeul Sud', 0, 'Mairie de Yeumbeul Sud', NULL, 'CA. YEUMBEUL SUD_CA', NULL, 1),
		('MAIRIE_YEUMNORD', 'Mairie de Yeumbeul Nord', 0, 'Mairie de Yeumbeul Nord', NULL, 'CA. YEUMBEUL NORD_CA', NULL, 1),
		('MAIRIE_YOFF', 'Mairie de Yoff', 0, 'Mairie de Yoff', NULL, 'CA. YOFF_CA', NULL, 1),
		('PREFET_RUFISQUE', '', 0, 'Préfecture Rufisque', NULL, 'RUFISQUE_PREFECTURE', 'NS', 1),
		('SOUSPREFECTURE_RUFISQUEEST', 'Sous Préfecture Rufisque EST', 0, 'Sous Préfecture Rufisque EST', NULL, 'RUFISQUE_ARRONDISSEMENT', 'NS', 1),
		('SOUSPREF_ALMADIES', 'Sous-préfecture des Almadies', 0, 'Sous-préfecture des Almadies', NULL, 'ALMADIES_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_BAMBYLOR', 'Sous-préfecture de Bambylor', 0, 'Sous-préfecture de Bambylor', NULL, 'BAMBYLOR_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_BARGNY', 'Sous-préfecture de Bargny', 0, 'Sous-préfecture de Bargny', NULL, 'BARGNY_ARRONDISSEMENT', NULL, 0),
		('SOUSPREF_DAGOUDANE', 'Sous-prefecture de Dagoudane', 0, 'Sous-prefecture de Dagoudane', NULL, 'PIKINE DAGOUDANE_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_DAKARPLATEAU', 'Sous-préfecture de Dakar-Plateau', 0, 'Sous-préfecture de Dakar-Plateau', NULL, 'DAKAR-PLATEAU_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_GRDAK', 'sous-prefecture de grand dakar', 0, 'sous-prefecture de grand dakar', NULL, 'GRAND DAKAR_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_GUEDIAWAYE', 'Sous-préfecture de Guédiawaye', 0, 'Sous-préfecture de Guédiawaye', NULL, 'GUEDIAWAYE_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_NIAYES', 'Sous-préfecture des niayes', 0, 'Sous-préfecture des niayes', NULL, 'NIAYES_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_PA', 'Sous-prefecture des PA', 0, 'Sous-prefecture des PA', NULL, 'PARCELLES ASSAINIES_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_PARCELLES', 'Sous_prefecture parcelles assainies', 0, 'Sous_prefecture parcelles assainies', 'SOUSPREF_ALMADIES', 'PARCELLES ASSAINIES_ARRONDISSEMENT', NULL, 1),
		('SOUSPREF_PARCELLES ASSAINIES', 'Sous_prefecture des Parcelles Assainies', 0, 'Sous_prefecture des Parcelles Assainies', 'SOUSPREF_GUEDIAWAYE', NULL, NULL, 1),
		('SOUSPREF_SANGALKAM', 'Sous Préfecture SANGALKAM', 0, 'Sous Préfecture SANGALKAM', NULL, 'SANGALKAM_ARRONDISSEMENT', 'NS', 1),
		('SOUSPREF_THIAROYE', 'Sous-préfecture de Thiaroye', 0, 'Sous-préfecture de Thiaroye', NULL, 'THIAROYE_ARRONDISSEMENT', NULL, 1),
		('SP_PKG', 'Sapeur Pompier Pikine/guediawaye', 1, 'Sapeur Pompier Pikine/guediawaye', NULL, 'GUEDIAWAYE', 'NS', 1),
		('tresor', 'DGCPT', 1, 'Tresor', NULL, NULL, 'NS', 1);
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;

/*DESACTIVATION CADASTRE ET DOMAINE DE GUEDIAWAYE ET PIKINE*/
	UPDATE  `organisations` SET  `ORG_STATUT` ='1';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'CG';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'CP';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'DG';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'DP';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'MG';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'PG';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'SPG';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'SPP';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'PP';
	UPDATE  `organisations` SET  `ORG_STATUT` ='0' WHERE  `organisations`.`ORG_CODE` = 'MP';



INSERT INTO `permissions` (`PERM_ID`, `PERM_NAME`, `PERM_STATUT`, `PERM_ETA_CODE`, `PERM_TRS_ID`) VALUES
	
   /*(89, 'Permission de Réintroduire', 1, 'COMPLETED', 89),non existant en demo mais existe en production, a valider*/
	(96, 'Permission de Renseigner Taxe', 1, 'SOUMIS_PAIEMENT', 96),
	(97, 'Permission de Visualiser', 1, 'SOUMIS_PAIEMENT', 97),
	(98, 'Permission de Payer', 1, 'EN_ATTENTE_PAIEMENT', 98),
	(99, 'Permission de Visualiser', 1, 'EN_ATTENTE_PAIEMENT', 99),
	(100, 'Permission de Accepter avec transfert', 1, 'PENDING', 100),
	(101, 'Permission de Approuver avec transfert', 1, 'COMPLETED', 101),
	(102, 'Permission de Demander avis DUA', 1, 'COMPLETED', 102),
	(103, 'Permission de Valider', 1, 'EN_ATTENTE_AVIS_DUA', 103),
	(104, 'Permission de Rejeter', 1, 'EN_ATTENTE_AVIS_DUA', 104),
	(105, 'Permission de Visualiser', 1, 'EN_ATTENTE_AVIS_DUA', 105),
	(106, 'Permission de Visualiser', 1, 'REJET_DUA', 106),
	(107, 'Permission de Visualiser', 1, 'VALIDE_DUA', 107),
	(108, 'Permission de Accepter', 1, 'REJET_DUA', 108),
	(109, 'Permission de Rejeter définitivement', 1, 'REJET_DUA', 109),
	(110, 'Permission de Suspendre', 1, 'REJET_DUA', 110),
	(111, 'Permission de Réintroduire', 1, 'REJET_DUA', 111),
	(112, 'Permission de Accepter', 1, 'VALIDE_DUA', 112),
	(113, 'Permission de Rejeter définitivement', 1, 'VALIDE_DUA', 113),
	(114, 'Permission de Suspendre', 1, 'VALIDE_DUA', 114),
	(115, 'Permission de Réintroduire', 1, 'VALIDE_DUA', 115),
	(116, 'Permission de Imputer', 1, 'EN_ATTENTE_AVIS_DUA', 116),
	(117, 'Permission de Approuver avec transfert', 1, 'REJET_DUA', 117),
	(118, 'Permission de Approuver avec transfert', 1, 'VALIDE_DUA', 118);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

INSERT INTO `fonctions` (`FCT_CODE`, `FCT_DESCRIPTION`, `FCT_LIBELLE`, `FCT_STATUT`, `FCT_WELCOME_BOOKMARK`, `FCT_ORG_CODE`) VALUES
	('AGENT_PAIEMENT', 'Agent Paiement SRUD', 'Agent Paiement SRUD', 0, '', 'SRUD'),
	('AGENT_PAIEMENT_DOM_DP', 'Agent Paiement Domaine DP', 'Agent Paiement Domaine DP', 0, '', 'DOM_DP'),
	('AGENT_PAIEMENT_MAIRIE', 'Agent Paiement Mairie', 'Agent Paiement Mairie', 0, '', 'MD'),
	('AGENT_SONATEL', 'Agent sonatel', 'Agent Sonatel', 1, 'gestion_fonctions', 'SONATEL'),
	('AG_RECETTE_BAMBYLOR', 'Agent recette Bambylor', 'Agent recette Bambylor', 1, 'gestion_fonctions', 'CABMB'),
	('AG_RECETTE_BARGNY', 'Agent Recette Bargny', 'Agent Recette Bargny', 1, 'gestion_fonctions', 'MAIRIE_BARGNY'),
	('AG_RECETTE_BISCUITERIE', 'Agent recette biscuiterie', 'Agent recette biscuiterie', 1, '', 'MAIRIE_BISCUITERIE'),
	('AG_RECETTE_CAMBERENE', 'Agent recette Camberene', 'Agent recette Camberene', 1, 'gestion_fonctions', 'MAIRIE_CAMBERENE'),
	('AG_RECETTE_COLOBANE', 'Agent recette Colobane', 'Agent recette Colobane', 1, 'gestion_fonctions', 'MAIRIE_COLOBANE'),
	('AG_RECETTE_DALIFORT', 'Agent recette Dalifort', 'Agent recette Dalifort', 1, 'gestion_fonctions', 'MAIRIE_DALIFORT'),
	('AG_RECETTE_DIACKSAO', 'Agent recette Diack Sao', 'Agent recette Diack Sao', 1, 'gestion_fonctions', 'MAIRIE_DIACKSAO'),
	('AG_RECETTE_DIAMEGUENE', 'Agent recette diameguene', 'Agent recette diameguene', 1, 'gestion_fonctions', 'MAIRIE_DIAMEGUENE'),
	('AG_RECETTE_DIAMNIADIO', 'Agent recette Diamniadio', 'Agent recette Diamniadio', 1, 'gestion_fonctions', 'CABMD'),
	('AG_RECETTE_DIEUPEUL', 'Agent recette dieupeul', 'Agent recette dieupeul', 1, 'gestion_fonctions', 'MAIRIE_DIEUPPEUL'),
	('AG_RECETTE_DJIDAH_THIAROYE_KAO', 'Agent recette Djidah Thiaroye Kao', 'Agent recette Djidah Thiaroye Kao', 1, 'gestion_fonctions', 'MAIRIE_THIAROYEKAO'),
	('AG_RECETTE_FANN_POINT_E_AMITIE', 'Agent recette Fann Point E Amitié', 'Agent recette Fann Point E Amitié', 1, 'gestion_fonctions', 'MAIRIE_FANN'),
	('AG_RECETTE_GOLF_SUD', 'Agent recette Golf Sud', 'Agent recette Golf Sud', 1, 'gestion_fonctions', 'MAIRIE_GOLFSUD'),
	('AG_RECETTE_GOREE', 'Agent recette Gorée', 'Agent recette Gorée', 1, 'gestion_fonctions', 'MAIRIE_GOREE'),
	('AG_RECETTE_GRAND_DAKAR', 'Agent recette Grand Dakar', 'Agent recette Grand Dakar', 1, 'gestion_fonctions', 'MAIRIE_GRANDAKAR'),
	('AG_RECETTE_GRDYOFF', 'Agent recette Grand Yoff', 'Agent recette Grand Yoff', 1, 'gestion_fonctions', 'MAIRIE_GRANDYOFF'),
	('AG_RECETTE_GUINAW_RAIL_NORD', 'Agent recette Guinaw rail Nord', 'Agent recette Guinaw rail Nord', 1, 'gestion_fonctions', 'MAIRIE_GUINORD'),
	('AG_RECETTE_GUINAW_RAIL_SUD', 'Agent recette Guinaw rail Sud', 'Agent recette Guinaw rail Sud', 1, 'gestion_fonctions', 'MAIRIE_GUISUD'),
	('AG_RECETTE_HANN_BELAIR', 'Agent recette Hann Belair', 'Agent recette Hann Belair', 1, 'gestion_fonctions', 'MAIRIE_HANN'),
	('AG_RECETTE_HLM', 'Agent recette Hlm', 'Agent recette Hlm', 1, 'gestion_fonctions', 'MAIRIE_HLM'),
	('AG_RECETTE_KEUR_MASSAR', 'Agent recette Keur Massa', 'Agent recette Keur Massar', 1, 'gestion_fonctions', 'MAIRIE_KEURMASSAR'),
	('AG_RECETTE_MALIKA', 'Agent recette Malika', 'Agent recette Malika', 1, 'gestion_fonctions', 'MAIRIE_MALIKA'),
	('AG_RECETTE_MBAO', 'Agent recette Mbao', 'Agent recette Mbao', 1, 'gestion_fonctions', 'MAIRIE_MBAO'),
	('AG_RECETTE_MEDINA', 'Agent recette Medina', 'Agent recette Medina', 1, 'gestion_fonctions', 'MAIRIE_MEDINA'),
	('AG_RECETTE_MEDINA_GOUNASS', 'Agent recette Medina Gounass', 'Agent recette Medina Gounass', 1, 'gestion_fonctions', 'MAIRIE_MEDGOUNASS'),
	('AG_RECETTE_MERMOZ', 'Agent Recette Mermoz', 'Agent Recette Mermoz', 1, 'gestion_fonctions', 'MAIRIE_MERMOZ'),
	('AG_RECETTE_NDIAREME_LIMAMOULAYE', 'Agent recette Ndiareme Limamoulaye', 'Agent recette Ndiareme Limamoulaye', 1, 'gestion_fonctions', 'MAIRIE_LIMAMOULAYE'),
	('AG_RECETTE_NGOR', 'Agent recette ngor', 'Agent recette ngor', 1, 'gestion_fonctions', 'MAIRIE_NGOR'),
	('AG_RECETTE_NIACOU', 'Agent recette niacoul rab', 'Agent recette niacoul rab', 1, 'gestion_fonctions', 'CABMJN'),
	('AG_RECETTE_OUAKAM', 'Agent recette Ouakam', 'Agent recette Ouakam', 1, 'gestion_fonctions', 'MAIRIE_OUAKAM'),
	('AG_RECETTE_PARCELLES', 'Agent recette Parcelles', 'Agent recette Parcelles', 1, 'gestion_fonctions', 'MAIRIE_PA'),
	('AG_RECETTE_PATTEDOIE', 'Agent recette Patte d\'Oie', 'Agent recette Patte d\'Oie', 1, 'gestion_fonctions', 'MAIRIE_PATTEDOIE'),
	('AG_RECETTE_PIKINE_EST', 'Agent recette Pikine Est', 'Agent recette Pikine Est', 1, 'gestion_fonctions', 'MAIRIE_PIKEST'),
	('AG_RECETTE_PLATEAU', 'Agent recette Plateau', 'Agent recette Plateau', 1, 'gestion_fonctions', 'MAIRIE_PLATEAU'),
	('AG_RECETTE_RUFEST', 'Agent recette Rufisque Est', 'Agent recette Rufisque Est', 1, 'gestion_fonctions', 'MAIRIE_RUFISQUE_EST'),
	('AG_RECETTE_RUFNORD', 'Agent recette Rufisque Nord', 'Agent recette Rufisque Nord', 1, 'gestion_fonctions', 'MAIRIE_RUFISQUE_NORD'),
	('AG_RECETTE_RUFOUEST', 'Agent recette Rufisque Ouest', 'Agent recette Rufisque Ouest', 1, 'gestion_fonctions', 'MAIRIE_RUFISQUE_OUEST'),
	('AG_RECETTE_SAM_NOTAIRE', 'Agent recette Sam Notaire', 'Agent recette Sam Notaire', 1, 'gestion_fonctions', 'MAIRIE_NOTAIRE'),
	('AG_RECETTE_SANGALKAM', 'Agent recette Sangalkam', 'Agent recette Sangalkam', 1, 'gestion_fonctions', 'CABMCS'),
	('AG_RECETTE_SEBIKOTANE', 'Agent recette Sébikotane', 'Agent recette Sébikotane', 1, 'gestion_fonctions', 'CABMSEB'),
	('AG_RECETTE_SENDOU', 'Agent recette Sendou', 'Agent recette Sendou', 1, 'gestion_fonctions', 'CABMSE'),
	('AG_RECETTE_SICAP_LIBERTE', 'Agent recette Sicap Liberté', 'Agent recette Sicap Liberté', 1, 'gestion_fonctions', 'MAIRIE_SICAP'),
	('AG_RECETTE_THIAROYE_GARE', 'Agent recette Thiaroye Gare', 'Agent recette Thiaroye Gare', 1, 'gestion_fonctions', 'MAIRIE_THIAGARE'),
	('AG_RECETTE_THIAROYE_MER', 'Agent recette Thiaroye Mer', 'Agent recette Thiaroye Mer', 1, 'gestion_fonctions', 'MAIRIE_THIAMER'),
	('AG_RECETTE_TIV_PEULH_NIAGUE', 'Agent recette Tivaouane Peulh Niague', 'Agent recette Tivaouane Peulh Niague', 1, 'gestion_fonctions', 'CABMTP'),
	('AG_RECETTE_WAKHINANE', 'Agent Recette Wakhinane', 'Agent Recette Wakhinane', 1, 'gestion_fonctions', 'MAIRIE_WAKHINANE'),
	('AG_RECETTE_YENE', 'Agent recette yene', 'Agent recette yene', 1, 'gestion_fonctions', 'CABMY'),
	('AG_RECETTE_YEUMNORD', 'Agent Recette Yeumbenul Nord', 'Agent Recette Yeumbenul Nord', 1, 'gestion_fonctions', 'MAIRIE_YEUMNORD'),
	('AG_RECETTE_YEUMSUD', 'Agent Recette Yeumbeul Sud', 'Agent Recette Yeumbeul Sud', 1, 'gestion_fonctions', 'MAIRIE_YEUMBEULSUD'),
	('AG_RECETTE_YOFF', 'Agent Recette Yoff', 'Agent Recette Yoff', 1, 'gestion_fonctions', 'MAIRIE_YOFF'),
	('ARCVTRESOR', 'Agent recouvrement tresor', 'Agent recouvrement tresor', 1, '', 'tresor'),
	('BUR_REC_DAKLIB', 'Agent du bureau de recouvrement de Dakar Liberté', 'Agent du bureau de recouvrement de Dakar Liberté', 1, 'gestion_fonctions', 'DOM_NAG'),
	('BUR_REC_DKPLA', 'Agent du bureau de recouvrement de Dakar Plateau', 'Agent du bureau de recouvrement de Dakar Plateau', 1, 'gestion_fonctions', 'DOM_NAG'),
	('BUR_REC_GRDAK', 'Agent du bureau de recouvrement de Grand Dakar', 'Agent du bureau de recouvrement de Grand Dakar', 1, 'gestion_fonctions', 'DOM_NAG'),
	('BUR_REC_NGAL', 'Agent du bureau de recouvrement de Ngor Almadies', 'Agent du bureau de recouvrement de Ngor Almadies', 1, 'gestion_fonctions', 'DOM_NAG'),
	('BUR_REC_PA', 'Agent du bureau de recouvrement des PA', 'Agent du bureau de recouvrement des PA', 1, 'gestion_fonctions', 'DOM_NAG'),
	('BUR_REC_PKG', 'Agent du bureau de recouvrement Pikine/Guédiawaye', 'Agent du bureau de recouvrement Pikine/Guédiawaye', 1, 'gestion_fonctions', 'DOM_PG'),
	('BUR_REC_RUF ', 'Agent du bureau de recouvrement de Rufisque', 'Agent du bureau de recouvrement de Rufisque', 1, 'gestion_fonctions', 'DR'),
	('CAB_MAIRIE_BAMBYLOR', 'Cabinet de la mairie de Bambylor', 'Cabinet de la mairie de Bambylor', 1, 'gestion_fonctions', 'CABMB'),
	('CAB_MAIRIE_BARGNY', 'Cabinet de la Mairie de Bargny', 'Cabinet de la Mairie de Bargny', 1, 'gestion_fonctions', 'MAIRIE_BARGNY'),
	('CAB_MAIRIE_BISCUITERIE', 'Cabinet de la mairie de Biscuiterie', 'Cabinet de la mairie de Biscuiterie', 1, 'gestion_fonctions', 'MAIRIE_BISCUITERIE'),
	('CAB_MAIRIE_CAMBERENE', 'Cabinet de la mairie de camberene', 'Cabinet de la mairie de camberene', 1, 'gestion_fonctions', 'MAIRIE_CAMBERENE'),
	('CAB_MAIRIE_COLOBANE', 'Cabinet de la Mairie de Colobane', 'Cabinet de la Mairie de Colobane', 1, 'gestion_fonctions', 'MAIRIE_COLOBANE'),
	('CAB_MAIRIE_DALIFORT', 'Cabinet de la mairie de dalifort', 'Cabinet de la mairie de dalifort', 1, 'gestion_fonctions', 'MAIRIE_DALIFORT'),
	('CAB_MAIRIE_DIACKSAO', 'Cabinet de la mairie de Diack Sao', 'Cabinet de la mairie de Diack Sao', 1, 'gestion_fonctions', 'MAIRIE_DIACKSAO'),
	('CAB_MAIRIE_DIAMEGUENE', 'Cabinet de la mairie de Diameguene', 'Cabinet de la mairie de Diameguene', 1, 'gestion_fonctions', 'MAIRIE_DIAMEGUENE'),
	('CAB_MAIRIE_DIAMNIADIO', 'Agent Cabinet Mairie Diamniadio ', 'Agent Cabinet Mairie Diamniadio ', 1, 'gestion_fonctions', 'CABMD'),
	('CAB_MAIRIE_DIEUPPEUL', 'Cabinet de la Mairie de dieuppeul', 'Cabinet de la Mairie de dieuppeul', 1, 'gestion_fonctions', 'MAIRIE_DIEUPPEUL'),
	('CAB_MAIRIE_FANN', 'Cabinet de la  Mairie de Fann', 'Cabinet de la  Mairie de Fann', 1, 'gestion_fonctions', 'MAIRIE_FANN'),
	('CAB_MAIRIE_GOLFSUD', 'Cabinet de la Mairie de Golf Sud', 'Cabinet de la Mairie de Golf Sud', 1, 'gestion_fonctions', 'MAIRIE_GOLFSUD'),
	('CAB_MAIRIE_GOREE', 'Cabinet de la Mairie de Goree', ' Cabinet de la Mairie de Goree', 1, 'gestion_fonctions', 'MAIRIE_GOREE'),
	('CAB_MAIRIE_GRANDAKAR', 'Cabinet de la mairie de grand dakar', 'Cabinet de la mairie de grand dakar', 1, 'gestion_fonctions', 'MAIRIE_GRANDAKAR'),
	('CAB_MAIRIE_GRANDYOFF', 'Cabinet de la Mairie de Grand Yoff', 'Cabinet de la Mairie de Grand Yoff', 1, 'gestion_fonctions', 'MAIRIE_GRANDYOFF'),
	('CAB_MAIRIE_GUINORD', 'Cabinet de la mairie de Guinaw rail Nord', 'Cabinet de la mairie de Guinaw rail Nord', 1, 'gestion_fonctions', 'MAIRIE_GUINORD'),
	('CAB_MAIRIE_GUISUD', 'Cabinet de la Mairie de Guinaw rail Sud', 'Cabinet de la Mairie de Guinaw rail Sud', 1, 'gestion_fonctions', 'MAIRIE_GUISUD'),
	('CAB_MAIRIE_HANN', 'Cabinet de la mairie de Hann', 'Cabinet de la mairie de Hann', 1, 'gestion_fonctions', 'MAIRIE_HANN'),
	('CAB_MAIRIE_HLM', 'Cabinet de la mairie de HLM', 'Cabinet de la mairie de HLM', 1, 'gestion_fonctions', 'MAIRIE_HLM'),
	('CAB_MAIRIE_JAXAAY', 'Cabinet Mairie Jaxaay Niacoul RAB', 'Cabinet Mairie Jaxaay Niacoul RAB', 1, 'gestion_fonctions', 'CABMJN'),
	('CAB_MAIRIE_KEURMASSAR', 'Cabinet de la Mairie de Keur Massar', 'Cabinet de la Mairie de Keur Massar', 1, 'gestion_fonctions', 'MAIRIE_KEURMASSAR'),
	('CAB_MAIRIE_LIMAMOULAYE', 'Cabinet de la mairie de limamoulaye', 'Cabinet de la mairie de limamoulaye', 1, 'gestion_fonctions', 'MAIRIE_LIMAMOULAYE'),
	('CAB_MAIRIE_MALIKA', 'Cabinet de la Mairie de Malika', 'Cabinet de la Mairie de Malika', 1, 'gestion_fonctions', 'MAIRIE_MALIKA'),
	('CAB_MAIRIE_MBAO', 'Cabinet de la Mairie de Mbao', 'Cabinet de la Mairie de Mbao', 1, 'gestion_fonctions', 'MAIRIE_MBAO'),
	('CAB_MAIRIE_MEDGOUNASS', 'Cabinet de la Mairie de Médina Gounass', 'Cabinet de la Mairie de Médina Gounass', 1, 'gestion_fonctions', 'MAIRIE_MEDGOUNASS'),
	('CAB_MAIRIE_MEDINA', 'Cabinet de la mairie de Médina', 'Cabinet de la mairie de Médina', 1, 'gestion_fonctions', 'MAIRIE_MEDINA'),
	('CAB_MAIRIE_MERMOZ', 'Cabinet de la Mairie de Mermoz', 'Cabinet de la Mairie de Mermoz', 1, 'gestion_fonctions', 'MAIRIE_MERMOZ'),
	('CAB_MAIRIE_NGOR', 'Cabinet de la mairie de Ngor', 'Cabinet de la mairie de Ngor', 1, 'gestion_fonctions', 'MAIRIE_NGOR'),
	('CAB_MAIRIE_NOTAIRE', 'Cabinet de la mairie de notaire', 'Cabinet de la mairie de notaire', 1, 'gestion_fonctions', 'MAIRIE_NOTAIRE'),
	('CAB_MAIRIE_OUAKAM', 'Cabinet de la mairie de Ouakam', 'Cabinet de la mairie de Ouakam', 1, 'gestion_fonctions', 'MAIRIE_OUAKAM'),
	('CAB_MAIRIE_PA', 'Cabinet de la Mairie des Parcelles', 'Cabinet de la Mairie des Parcelles', 1, 'gestion_fonctions', 'MAIRIE_PA'),
	('CAB_MAIRIE_PATTEDOIE', 'Cabinet de la Mairie de Patte d\'Oie', 'Cabinet de la Mairie de Patte d\'Oie', 1, 'gestion_fonctions', 'MAIRIE_PATTEDOIE'),
	('CAB_MAIRIE_PIKEST', 'Cabinet de la Mairie de pikine EST', 'Cabinet de la Mairie de pikine EST', 1, 'gestion_fonctions', 'MAIRIE_PIKEST'),
        ('CAB_MAIRIE_PIKOUEST', 'Cabinet de la Mairie de pikine Ouest', 'Cabinet de la Mairie de pikine Ouest', 1, 'gestion_fonctions', 'MAIRIE_PIKOUEST'),
        ('CAB_MAIRIE_PIKNORD', 'Cabinet de la Mairie de pikine Nord', 'Cabinet de la Mairie de pikine Nord', 1, 'gestion_fonctions', 'MAIRIE_PIKNORD'),
	('CAB_MAIRIE_PLATEAU', 'Cabinet de la mairie de Plateau', 'Cabinet de la mairie de Plateau', 1, 'gestion_fonctions', 'MAIRIE_PLATEAU'),
	('CAB_MAIRIE_RUFIQUE_OUEST', 'Cabinet Mairie Rufisque Ouest', 'Cabinet Mairie Rufisque Ouest', 1, 'gestion_fonctions', 'MAIRIE_RUFISQUE_OUEST'),
	('CAB_MAIRIE_RUFISK_EST', 'Cabinet Mairie Rufisque Est', 'Cabinet Mairie Rufisque Est', 1, 'gestion_fonctions', 'MAIRIE_RUFISQUE_EST'),
	('CAB_MAIRIE_RUFISQUE_EST', 'Cabinet Mairie Rufisque  Est', 'Cabinet Mairie Rufisque  Est', 0, 'gestion_fonctions', 'CABMRE'),
	('CAB_MAIRIE_RUFISQUE_NORD', 'Cabinet Mairie Rufisque Nord', 'Cabinet Mairie Rufisque Nord', 1, 'gestion_fonctions', 'MAIRIE_RUFISQUE_NORD'),
	('CAB_MAIRIE_SANGALKAM', 'Agent Cabinet Commune de  Sangalkam ', 'Agent Cabinet Commune de  Sangalkam ', 1, 'gestion_fonctions', 'CABMCS'),
	('CAB_MAIRIE_SEBIKOTANE', 'Cabinet Mairie de Sebikotane', 'Cabinet Mairie de Sebikotane', 1, '', 'CABMSEB'),
	('CAB_MAIRIE_SENDOU', 'Cabinet Mairie de Sendou', 'Cabinet Mairie de Sendou', 1, '', 'CABMSE'),
	('CAB_MAIRIE_SICAP', 'Cabinet de la mairie de Sicap', 'Cabinet de la mairie de Sicap', 1, 'gestion_fonctions', 'MAIRIE_SICAP'),
	('CAB_MAIRIE_THIAGARE', 'Cabinet de la Mairie de Thiaroye Gare', 'Cabinet de la Mairie de Thiaroye Gare', 1, 'gestion_fonctions', 'MAIRIE_THIAGARE'),
	('CAB_MAIRIE_THIAMER', 'Cabinet de la Mairie de Thiaroye Mer', 'Cabinet de la Mairie de Thiaroye Mer', 1, 'gestion_fonctions', 'MAIRIE_THIAMER'),
	('CAB_MAIRIE_THIAROYEKAO', 'Cabinet de la Mairie de Thiaroyekao', 'Cabinet de la Mairie de Thiaroyekao', 1, 'gestion_fonctions', 'MAIRIE_THIAROYEKAO'),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 'Cabinet Mairie Tivaoune Peulh Niague', 'Cabinet Mairie Tivaoune Peulh Niague', 1, 'gestion_fonctions', 'CABMTP'),
	('CAB_MAIRIE_WAKHINANE', 'cabinet mairie de Wakhinane nimzatt', 'cabinet mairie de Wakhinane nimzatt', 1, 'gestion_fonctions', 'MAIRIE_WAKHINANE'),
	('CAB_MAIRIE_YENE', 'Cabinet de la Mairie de Yene', 'Cabinet de la Mairie de Yene', 1, 'gestion_fonctions', 'CABMY'),
	('CAB_MAIRIE_YEUMNORD', 'Cabinet de la mairie de Yeumbeul Nord', 'Cabinet de la mairie de Yeumbeul Nord', 1, 'gestion_fonctions', 'MAIRIE_YEUMNORD'),
	('CAB_MAIRIE_YEUMSUD', 'Cabinet de la mairie de Yeumbeul Sud', 'Cabinet de la mairie de Yeumbeul Sud', 1, 'gestion_fonctions', 'MAIRIE_YEUMBEULSUD'),
	('CAB_MAIRIE_YOFF', 'Cabinet de la Mairie de Yoff', 'Cabinet de la Mairie de Yoff', 1, 'gestion_fonctions', 'MAIRIE_YOFF'),
	('DDUA', '', 'Directeur DUA', 1, '', 'DUA'),
	('FCT_SP_PKG', 'Sapeur Pompier Pikine/guediawaye', 'Sapeur Pompier Pikine/guediawaye', 1, '', 'SP_PKG'),
	('PREFET_RUFISQUE', '', 'Prefecture Rufisque', 1, 'gestion_fonctions', 'PREFET_RUFISQUE'),
	('SOUSPREF_RUFISQUEEST ', 'Sous Préfecture Rufisque EST', 'Sous Préfecture Rufisque EST', 1, 'gestion_fonctions', 'SOUSPREFECTURE_RUFISQUEEST'),
	('SOUSPREF_SANGALKAM', 'Sous Préfecture SANGALKAM', 'Sous-Préfet SANGALKAM', 1, 'gestion_fonctions', 'SOUSPREF_SANGALKAM'),
	('SOUS_PREF_ALMADIES', 'Sous-préfecture des Almadies', 'Sous-préfecture des Almadies', 1, 'gestion_fonctions', 'SOUSPREF_ALMADIES'),
	('SOUS_PREF_BAMBYLOR', 'Sous-préfecture de Bambylor', 'Sous-préfecture de Bambylor', 1, 'gestion_fonctions', 'SOUSPREF_BAMBYLOR'),
	('SOUS_PREF_BARGNY', 'Sous-préfecture de Bargny', 'Sous-préfecture de Bargny', 1, 'gestion_fonctions', 'SOUSPREF_BARGNY'),
	('SOUS_PREF_DAGOUDANE', 'Sous-prefecture de Dagoudane', 'Sous-prefecture de Dagoudane', 1, 'gestion_fonctions', 'SOUSPREF_DAGOUDANE'),
	('SOUS_PREF_DAKPLATEAU', 'Sous-préfecture de Dakar-Plateau', 'Sous-préfecture de Dakar-Plateau', 1, 'gestion_fonctions', 'SOUSPREF_DAKARPLATEAU'),
	('SOUS_PREF_GRDAK', 'Sous-prefecture de grand dakar', 'Sous-prefecture de grand dakar', 1, 'gestion_fonctions', 'SOUSPREF_GRDAK'),
	('SOUS_PREF_GUEDIAWAYE', 'Sous-préfecture de Guédiawaye', 'Sous-préfecture de Guédiawaye', 1, 'gestion_fonctions', 'SOUSPREF_GUEDIAWAYE'),
	('SOUS_PREF_NIAYES', 'Sous-préfecture des niayes', 'Sous-préfecture des niayes', 1, 'gestion_fonctions', 'SOUSPREF_NIAYES'),
	('SOUS_PREF_PA', 'sous-prefecture des PA', 'sous-prefecture des PA', 1, 'gestion_fonctions', 'SOUSPREF_PA'),
	('SOUS_PREF_THIAROYE', 'Sous-préfecture de Thiaroye', 'Sous-préfecture de Thiaroye', 1, 'gestion_fonctions', 'SOUSPREF_THIAROYE');

	
	UPDATE `fonctions` SET `FCT_STATUT`=0 WHERE  `FCT_CODE`='ASP_GW';
	UPDATE `fonctions` SET `FCT_STATUT`=0 WHERE  `FCT_CODE`='SDE';

/* deleted rows 
INSERT INTO `fonctions_permissions` (`FP_FCT_CODE`, `FP_PERM_ID`) VALUES
	
	('AC_GW', 81),
	('AD_GW', 81),
	('AD_Pikine', 81),
	('AC_Pikine', 81),
	('AC_GW', 74),
	('AD_GW', 74),
	('AC_Pikine', 74),
	('AD_Pikine', 74),

	('CSRUD', 88),
	('CSDU_GW', 88),
	('CSDUP', 88),
	('CSDU_RF', 88)

	('ACAD_PG', 74),
	('ACAD_PG', 81),
	('ADOM_PG', 74),
	('ADOM_PG', 81),
	('ABEL_GW', 74),
	('ABPC_GW', 74),
	('ASNH_GW', 74),
	('ASNH_GW', 81),
	('SOUS_PREF_BARGNY', 63),
	('SOUS_PREF_BARGNY', 51),
	('SOUS_PREF_BARGNY', 47),
	('SOUSPREF_SANGALKAM', 63),
	('SOUSPREF_SANGALKAM', 51),
	('SOUSPREF_SANGALKAM', 47),
		
	('SOUS_PREF_RUFISQUE_EST', 63),//dupliqué
	('SOUS_PREF_RUFISQUE_EST', 51),
	('SOUS_PREF_RUFISQUE_EST', 47), //dupliqué

	('ABPC_GW', 85),
	('ABPC_GW', 86),
	('ABPC_GW', 87),
	('ABPC_GW', 88),
	('ABPC_GW', 79),
	('ABPC_GW', 53),

	('CSRUD', 101),
	('CSRUD', 69),
	('CSRUD', 82),
	('CSRUD', 78),
	('CSDUP', 53),
	('CSDUP', 68),
	('CSDUP', 82),
	('CSDUP', 79),
	('CSDUP', 72),
	('CSRUD', 79),
	('CSDUP', 63),

	('CSDU_GW', 63),
	('CSDU_GW', 79),
	('CSDU_GW', 71),
	('CSDU_GW', 72),

	('CSDU_RF', 63),
	('CSDU_RF', 79),
	('CSDU_RF', 68),
	('CSDU_RF', 69),
	('CSDU_RF', 70),
	('CSDU_RF', 71),
	('CSDU_RF', 72),

	('CSDU_GW', 68),
	('CSDU_GW', 82),
	('CSDU_GW', 78),
	('CSDU_RF', 82),
	('CSDU_RF', 78),

	('DDUA', 103),
	('DDUA', 104),
	('DDUA', 105),
	('DDUA', 106),
	('DDUA', 107),
	('DDUA', 116),


	('ASDE', 40),
	('ASDE', 61),
	('CSDU_RF', 64),
	('CSDUP', 64),
	('CSDU_GW', 64),

	('ADUA', 105),
	('ADUA', 104),
	('ADUA', 103),
	('ADUA', 106),
	('ADUA', 107),
	('ADUA', 116),

	('CSDUP', 71),


	('DUA', 74),  //ne pas faire retour arrière
	('DUA', 81),  //ne pas faire retour arrière


	(294, 'DDUA', 63),
	(295, 'DDUA', 64),
	(296, 'DDUA', 65),
	(297, 'DDUA', 66),
	(299, 'DDUA', 68),
	(300, 'DDUA', 69),
	(301, 'DDUA', 70),
	(302, 'DDUA', 71),
	(303, 'DDUA', 72),
	
	(345, 'SDE', 40),
	(346, 'SDE', 61),
	(347, 'SDE', 59),
	(348, 'SDE', 62);
	*/
INSERT INTO `fonctions_permissions` (`FP_FCT_CODE`, `FP_PERM_ID`) VALUES

	('PREFET_RUFISQUE', 63),
	('PREFET_RUFISQUE', 51),
	('PREFET_RUFISQUE', 47),
	
	('SOUS_PREF_ALMADIES', 63),
	('SOUS_PREF_ALMADIES', 51),
	('SOUS_PREF_ALMADIES', 47),

	('SOUS_PREF_DAKPLATEAU', 63),
	('SOUS_PREF_DAKPLATEAU', 51),
	('SOUS_PREF_DAKPLATEAU', 47),

	('SOUS_PREF_BAMBYLOR', 63),
	('SOUS_PREF_BAMBYLOR', 51),
	('SOUS_PREF_BAMBYLOR', 47),

	('SOUS_PREF_NIAYES', 63),
	('SOUS_PREF_NIAYES', 51),
	('SOUS_PREF_NIAYES', 47),

	('SOUS_PREF_THIAROYE', 63),
	('SOUS_PREF_THIAROYE', 51),
	('SOUS_PREF_THIAROYE', 47),

	('SOUS_PREF_GRDAK', 63),
	('SOUS_PREF_GRDAK', 51),
	('SOUS_PREF_GRDAK', 47),

	('SOUS_PREF_DAGOUDANE', 63),
	('SOUS_PREF_DAGOUDANE', 51),
	('SOUS_PREF_DAGOUDANE', 47),

	('SOUSPREF_RUFISQUEEST ', 63), 
	('SOUSPREF_RUFISQUEEST ', 51),
	('SOUSPREF_RUFISQUEEST ', 47), 

	('SOUS_PREF_GUEDIAWAYE', 63),
	('SOUS_PREF_GUEDIAWAYE', 51),
	('SOUS_PREF_GUEDIAWAYE', 47),

	('SOUS_PREF_PA', 63),
	('SOUS_PREF_PA', 51),
	('SOUS_PREF_PA', 47),

	('CAB_MAIRIE_NGOR', 84),
	('CAB_MAIRIE_NGOR', 70),
	('CAB_MAIRIE_NGOR', 58),
	('CAB_MAIRIE_NGOR', 57),
	('CAB_MAIRIE_NGOR', 83),
	('CAB_MAIRIE_NGOR', 71),
	('CAB_MAIRIE_NGOR', 50),
	('CAB_MAIRIE_NGOR', 46),

	('CAB_MAIRIE_OUAKAM', 83),
	('CAB_MAIRIE_OUAKAM', 84),
	('CAB_MAIRIE_OUAKAM', 71),
	('CAB_MAIRIE_OUAKAM', 50),
	('CAB_MAIRIE_OUAKAM', 46),
	('CAB_MAIRIE_OUAKAM', 70),
	('CAB_MAIRIE_OUAKAM', 58),
	('CAB_MAIRIE_OUAKAM', 57),

	('CAB_MAIRIE_MEDINA', 83),
	('CAB_MAIRIE_MEDINA', 71),
	('CAB_MAIRIE_MEDINA', 50),
	('CAB_MAIRIE_MEDINA', 46),
	('CAB_MAIRIE_MEDINA', 84),
	('CAB_MAIRIE_MEDINA', 70),
	('CAB_MAIRIE_MEDINA', 58),
	('CAB_MAIRIE_MEDINA', 57),

	('CAB_MAIRIE_PLATEAU', 83),
	('CAB_MAIRIE_PLATEAU', 71),
	('CAB_MAIRIE_PLATEAU', 50),
	('CAB_MAIRIE_PLATEAU', 46),
	('CAB_MAIRIE_PLATEAU', 84),
	('CAB_MAIRIE_PLATEAU', 70),
	('CAB_MAIRIE_PLATEAU', 58),
	('CAB_MAIRIE_PLATEAU', 57),

	('CAB_MAIRIE_GOLFSUD', 83),
	('CAB_MAIRIE_GOLFSUD', 71),
	('CAB_MAIRIE_GOLFSUD', 50),
	('CAB_MAIRIE_GOLFSUD', 46),
	('CAB_MAIRIE_GOLFSUD', 84),
	('CAB_MAIRIE_GOLFSUD', 70),
	('CAB_MAIRIE_GOLFSUD', 58),
	('CAB_MAIRIE_GOLFSUD', 57),

	('CAB_MAIRIE_MEDGOUNASS', 83),
	('CAB_MAIRIE_MEDGOUNASS', 71),
	('CAB_MAIRIE_MEDGOUNASS', 50),
	('CAB_MAIRIE_MEDGOUNASS', 46),
	('CAB_MAIRIE_MEDGOUNASS', 84),
	('CAB_MAIRIE_MEDGOUNASS', 70),
	('CAB_MAIRIE_MEDGOUNASS', 58),
	('CAB_MAIRIE_MEDGOUNASS', 57),
	
	('CAB_MAIRIE_KEURMASSAR', 83),
	('CAB_MAIRIE_KEURMASSAR', 71),
	('CAB_MAIRIE_KEURMASSAR', 50),
	('CAB_MAIRIE_KEURMASSAR', 46),
	('CAB_MAIRIE_KEURMASSAR', 84),
	('CAB_MAIRIE_KEURMASSAR', 70),
	('CAB_MAIRIE_KEURMASSAR', 58),
	('CAB_MAIRIE_KEURMASSAR', 57),

	('CAB_MAIRIE_MALIKA', 83),
	('CAB_MAIRIE_MALIKA', 71),
	('CAB_MAIRIE_MALIKA', 50),
	('CAB_MAIRIE_MALIKA', 46),
	('CAB_MAIRIE_MALIKA', 84),
	('CAB_MAIRIE_MALIKA', 70),
	('CAB_MAIRIE_MALIKA', 58),
	('CAB_MAIRIE_MALIKA', 57),
	
	('CAB_MAIRIE_DIACKSAO', 83),
	('CAB_MAIRIE_DIACKSAO', 71),
	('CAB_MAIRIE_DIACKSAO', 50),
	('CAB_MAIRIE_DIACKSAO', 46),
	('CAB_MAIRIE_DIACKSAO', 84),
	('CAB_MAIRIE_DIACKSAO', 70),
	('CAB_MAIRIE_DIACKSAO', 58),
	('CAB_MAIRIE_DIACKSAO', 57),

	('CAB_MAIRIE_MBAO', 83),
	('CAB_MAIRIE_MBAO', 71),
	('CAB_MAIRIE_MBAO', 50),
	('CAB_MAIRIE_MBAO', 46),
	('CAB_MAIRIE_MBAO', 84),
	('CAB_MAIRIE_MBAO', 70),
	('CAB_MAIRIE_MBAO', 58),
	('CAB_MAIRIE_MBAO', 57),
	
	('CAB_MAIRIE_BAMBYLOR', 83),
	('CAB_MAIRIE_BAMBYLOR', 71),
	('CAB_MAIRIE_BAMBYLOR', 50),
	('CAB_MAIRIE_BAMBYLOR', 46),
	('CAB_MAIRIE_BAMBYLOR', 84),
	('CAB_MAIRIE_BAMBYLOR', 70),
	('CAB_MAIRIE_BAMBYLOR', 58),
	('CAB_MAIRIE_BAMBYLOR', 57),

	('CAB_MAIRIE_YENE', 84),
	('CAB_MAIRIE_YENE', 70),
	('CAB_MAIRIE_YENE', 58),
	('CAB_MAIRIE_YENE', 57),
	('CAB_MAIRIE_YENE', 83),
	('CAB_MAIRIE_YENE', 71),
	('CAB_MAIRIE_YENE', 50),
	('CAB_MAIRIE_YENE', 46),

	('CAB_MAIRIE_BARGNY', 84),
	('CAB_MAIRIE_BARGNY', 70),
	('CAB_MAIRIE_BARGNY', 58),
	('CAB_MAIRIE_BARGNY', 57),
	('CAB_MAIRIE_BARGNY', 83),
	('CAB_MAIRIE_BARGNY', 71),
	('CAB_MAIRIE_BARGNY', 50),
	('CAB_MAIRIE_BARGNY', 46),

	
	('CAB_MAIRIE_YOFF', 46),
	('CAB_MAIRIE_YOFF', 50),
	('CAB_MAIRIE_YOFF', 83),
	('CAB_MAIRIE_YOFF', 71),
	('CAB_MAIRIE_YOFF', 84),
	('CAB_MAIRIE_YOFF', 70),
	('CAB_MAIRIE_YOFF', 58),
	('CAB_MAIRIE_YOFF', 57),

	('CAB_MAIRIE_COLOBANE', 46),
	('CAB_MAIRIE_COLOBANE', 50),
	('CAB_MAIRIE_COLOBANE', 83),
	('CAB_MAIRIE_COLOBANE', 71),
	('CAB_MAIRIE_COLOBANE', 57),
	('CAB_MAIRIE_COLOBANE', 84),
	('CAB_MAIRIE_COLOBANE', 70),
	('CAB_MAIRIE_COLOBANE', 58),

	('CAB_MAIRIE_MERMOZ', 83),
	('CAB_MAIRIE_MERMOZ', 71),
	('CAB_MAIRIE_MERMOZ', 50),
	('CAB_MAIRIE_MERMOZ', 46),
	('CAB_MAIRIE_MERMOZ', 84),
	('CAB_MAIRIE_MERMOZ', 70),
	('CAB_MAIRIE_MERMOZ', 58),
	('CAB_MAIRIE_MERMOZ', 57),

	('CAB_MAIRIE_GOREE', 46),
	('CAB_MAIRIE_GOREE', 83),
	('CAB_MAIRIE_GOREE', 71),
	('CAB_MAIRIE_GOREE', 50),
	('CAB_MAIRIE_GOREE', 84),
	('CAB_MAIRIE_GOREE', 70),
	('CAB_MAIRIE_GOREE', 58),
	('CAB_MAIRIE_GOREE', 57),

	('CAB_MAIRIE_FANN', 46),
	('CAB_MAIRIE_FANN', 83),
	('CAB_MAIRIE_FANN', 71),
	('CAB_MAIRIE_FANN', 50),
	('CAB_MAIRIE_FANN', 84),
	('CAB_MAIRIE_FANN', 70),
	('CAB_MAIRIE_FANN', 58),
	('CAB_MAIRIE_FANN', 57),

	('CAB_MAIRIE_NOTAIRE', 83),
	('CAB_MAIRIE_NOTAIRE', 71),
	('CAB_MAIRIE_NOTAIRE', 50),
	('CAB_MAIRIE_NOTAIRE', 46),
	('CAB_MAIRIE_NOTAIRE', 84),
	('CAB_MAIRIE_NOTAIRE', 70),
	('CAB_MAIRIE_NOTAIRE', 58),
	('CAB_MAIRIE_NOTAIRE', 57),

	('CAB_MAIRIE_LIMAMOULAYE', 46),
	('CAB_MAIRIE_LIMAMOULAYE', 83),
	('CAB_MAIRIE_LIMAMOULAYE', 71),
	('CAB_MAIRIE_LIMAMOULAYE', 50),
	('CAB_MAIRIE_LIMAMOULAYE', 84),
	('CAB_MAIRIE_LIMAMOULAYE', 70),
	('CAB_MAIRIE_LIMAMOULAYE', 58),
	('CAB_MAIRIE_LIMAMOULAYE', 57),

	('CAB_MAIRIE_WAKHINANE', 83),
	('CAB_MAIRIE_WAKHINANE', 71),
	('CAB_MAIRIE_WAKHINANE', 50),
	('CAB_MAIRIE_WAKHINANE', 46),
	('CAB_MAIRIE_WAKHINANE', 84),
	('CAB_MAIRIE_WAKHINANE', 70),
	('CAB_MAIRIE_WAKHINANE', 58),
	('CAB_MAIRIE_WAKHINANE', 57),

	('CAB_MAIRIE_YEUMNORD', 83),
	('CAB_MAIRIE_YEUMNORD', 71),
	('CAB_MAIRIE_YEUMNORD', 50),
	('CAB_MAIRIE_YEUMNORD', 46),
	('CAB_MAIRIE_YEUMNORD', 84),
	('CAB_MAIRIE_YEUMNORD', 70),
	('CAB_MAIRIE_YEUMNORD', 58),
	('CAB_MAIRIE_YEUMNORD', 57),

	('CAB_MAIRIE_YEUMSUD', 83),
	('CAB_MAIRIE_YEUMSUD', 71),
	('CAB_MAIRIE_YEUMSUD', 50),
	('CAB_MAIRIE_YEUMSUD', 46),
	('CAB_MAIRIE_YEUMSUD', 84),
	('CAB_MAIRIE_YEUMSUD', 70),
	('CAB_MAIRIE_YEUMSUD', 58),
	('CAB_MAIRIE_YEUMSUD', 57),

	('CAB_MAIRIE_DIAMEGUENE', 83),
	('CAB_MAIRIE_DIAMEGUENE', 71),
	('CAB_MAIRIE_DIAMEGUENE', 50),
	('CAB_MAIRIE_DIAMEGUENE', 46),
	('CAB_MAIRIE_DIAMEGUENE', 84),
	('CAB_MAIRIE_DIAMEGUENE', 70),
	('CAB_MAIRIE_DIAMEGUENE', 58),
	('CAB_MAIRIE_DIAMEGUENE', 57),

	('CAB_MAIRIE_THIAMER', 83),
	('CAB_MAIRIE_THIAMER', 71),
	('CAB_MAIRIE_THIAMER', 50),
	('CAB_MAIRIE_THIAMER', 46),
	('CAB_MAIRIE_THIAMER', 84),
	('CAB_MAIRIE_THIAMER', 70),
	('CAB_MAIRIE_THIAMER', 58),
	('CAB_MAIRIE_THIAMER', 57),

	('CAB_MAIRIE_THIAGARE', 83),
	('CAB_MAIRIE_THIAGARE', 71),
	('CAB_MAIRIE_THIAGARE', 50),
	('CAB_MAIRIE_THIAGARE', 46),
	('CAB_MAIRIE_THIAGARE', 84),
	('CAB_MAIRIE_THIAGARE', 70),
	('CAB_MAIRIE_THIAGARE', 58),
	('CAB_MAIRIE_THIAGARE', 57),


	('CAB_MAIRIE_CAMBERENE', 83),
	('CAB_MAIRIE_CAMBERENE', 71),
	('CAB_MAIRIE_CAMBERENE', 50),
	('CAB_MAIRIE_CAMBERENE', 46),
	('CAB_MAIRIE_CAMBERENE', 84),
	('CAB_MAIRIE_CAMBERENE', 70),
	('CAB_MAIRIE_CAMBERENE', 58),
	('CAB_MAIRIE_CAMBERENE', 57),

	('CAB_MAIRIE_GRANDYOFF', 83),
	('CAB_MAIRIE_GRANDYOFF', 71),
	('CAB_MAIRIE_GRANDYOFF', 50),
	('CAB_MAIRIE_GRANDYOFF', 46),
	('CAB_MAIRIE_GRANDYOFF', 84),
	('CAB_MAIRIE_GRANDYOFF', 70),
	('CAB_MAIRIE_GRANDYOFF', 58),
	('CAB_MAIRIE_GRANDYOFF', 57),

	('CAB_MAIRIE_PA', 83),
	('CAB_MAIRIE_PA', 71),
	('CAB_MAIRIE_PA', 50),
	('CAB_MAIRIE_PA', 46),
	('CAB_MAIRIE_PA', 84),
	('CAB_MAIRIE_PA', 70),
	('CAB_MAIRIE_PA', 58),
	('CAB_MAIRIE_PA', 57),

	('CAB_MAIRIE_PATTEDOIE', 83),
	('CAB_MAIRIE_PATTEDOIE', 71),
	('CAB_MAIRIE_PATTEDOIE', 50),
	('CAB_MAIRIE_PATTEDOIE', 46),
	('CAB_MAIRIE_PATTEDOIE', 84),
	('CAB_MAIRIE_PATTEDOIE', 70),
	('CAB_MAIRIE_PATTEDOIE', 58),
	('CAB_MAIRIE_PATTEDOIE', 57),

	
	('CAB_MAIRIE_DALIFORT', 83),
	('CAB_MAIRIE_DALIFORT', 71),
	('CAB_MAIRIE_DALIFORT', 50),
	('CAB_MAIRIE_DALIFORT', 46),
	('CAB_MAIRIE_DALIFORT', 84),
	('CAB_MAIRIE_DALIFORT', 70),
	('CAB_MAIRIE_DALIFORT', 58),
	('CAB_MAIRIE_DALIFORT', 57),

	('CAB_MAIRIE_BISCUITERIE', 83),
	('CAB_MAIRIE_BISCUITERIE', 71),
	('CAB_MAIRIE_BISCUITERIE', 50),
	('CAB_MAIRIE_BISCUITERIE', 46),
	('CAB_MAIRIE_BISCUITERIE', 84),
	('CAB_MAIRIE_BISCUITERIE', 70),
	('CAB_MAIRIE_BISCUITERIE', 58),
	('CAB_MAIRIE_BISCUITERIE', 57),

	('CAB_MAIRIE_DIEUPPEUL', 83),
	('CAB_MAIRIE_DIEUPPEUL', 71),
	('CAB_MAIRIE_DIEUPPEUL', 50),
	('CAB_MAIRIE_DIEUPPEUL', 46),
	('CAB_MAIRIE_DIEUPPEUL', 84),
	('CAB_MAIRIE_DIEUPPEUL', 70),
	('CAB_MAIRIE_DIEUPPEUL', 58),
	('CAB_MAIRIE_DIEUPPEUL', 57),

	('CAB_MAIRIE_GRANDAKAR', 83),
	('CAB_MAIRIE_GRANDAKAR', 71),
	('CAB_MAIRIE_GRANDAKAR', 50),
	('CAB_MAIRIE_GRANDAKAR', 46),
	('CAB_MAIRIE_GRANDAKAR', 84),
	('CAB_MAIRIE_GRANDAKAR', 70),
	('CAB_MAIRIE_GRANDAKAR', 58),
	('CAB_MAIRIE_GRANDAKAR', 57),

	('CAB_MAIRIE_SICAP', 83),
	('CAB_MAIRIE_SICAP', 71),
	('CAB_MAIRIE_SICAP', 50),
	('CAB_MAIRIE_SICAP', 46),
	('CAB_MAIRIE_SICAP', 84),
	('CAB_MAIRIE_SICAP', 70),
	('CAB_MAIRIE_SICAP', 58),
	('CAB_MAIRIE_SICAP', 57),

	('CAB_MAIRIE_HANN', 83),
	('CAB_MAIRIE_HANN', 71),
	('CAB_MAIRIE_HANN', 50),
	('CAB_MAIRIE_HANN', 46),
	('CAB_MAIRIE_HANN', 84),
	('CAB_MAIRIE_HANN', 70),
	('CAB_MAIRIE_HANN', 58),
	('CAB_MAIRIE_HANN', 57),

	('CAB_MAIRIE_HLM', 83),
	('CAB_MAIRIE_HLM', 71),
	('CAB_MAIRIE_HLM', 50),
	('CAB_MAIRIE_HLM', 46),
	('CAB_MAIRIE_HLM', 84),
	('CAB_MAIRIE_HLM', 70),
	('CAB_MAIRIE_HLM', 58),
	('CAB_MAIRIE_HLM', 57),

	('CAB_MAIRIE_THIAROYEKAO', 83),
	('CAB_MAIRIE_THIAROYEKAO', 71),
	('CAB_MAIRIE_THIAROYEKAO', 50),
	('CAB_MAIRIE_THIAROYEKAO', 46),
	('CAB_MAIRIE_THIAROYEKAO', 84),
	('CAB_MAIRIE_THIAROYEKAO', 70),
	('CAB_MAIRIE_THIAROYEKAO', 58),
	('CAB_MAIRIE_THIAROYEKAO', 57),

	('CAB_MAIRIE_GUINORD', 83),
	('CAB_MAIRIE_GUINORD', 71),
	('CAB_MAIRIE_GUINORD', 50),
	('CAB_MAIRIE_GUINORD', 46),
	('CAB_MAIRIE_GUINORD', 84),
	('CAB_MAIRIE_GUINORD', 70),
	('CAB_MAIRIE_GUINORD', 58),
	('CAB_MAIRIE_GUINORD', 57),

	('CAB_MAIRIE_GUISUD', 83),
	('CAB_MAIRIE_GUISUD', 71),
	('CAB_MAIRIE_GUISUD', 50),
	('CAB_MAIRIE_GUISUD', 46),
	('CAB_MAIRIE_GUISUD', 84),
	('CAB_MAIRIE_GUISUD', 70),
	('CAB_MAIRIE_GUISUD', 58),
	('CAB_MAIRIE_GUISUD', 57),

	('CAB_MAIRIE_PIKEST', 83),
	('CAB_MAIRIE_PIKEST', 71),
	('CAB_MAIRIE_PIKEST', 50),
	('CAB_MAIRIE_PIKEST', 46),
	('CAB_MAIRIE_PIKEST', 84),
	('CAB_MAIRIE_PIKEST', 70),
	('CAB_MAIRIE_PIKEST', 58),
	('CAB_MAIRIE_PIKEST', 57),    

        ('CAB_MAIRIE_PIKOUEST', 83),
	('CAB_MAIRIE_PIKOUEST', 71),
	('CAB_MAIRIE_PIKOUEST', 50),
	('CAB_MAIRIE_PIKOUEST', 46),
	('CAB_MAIRIE_PIKOUEST', 84),
	('CAB_MAIRIE_PIKOUEST', 70),
	('CAB_MAIRIE_PIKOUEST', 58),
	('CAB_MAIRIE_PIKOUEST', 57),
        
    ('CAB_MAIRIE_PIKNORD', 83),
	('CAB_MAIRIE_PIKNORD', 71),
	('CAB_MAIRIE_PIKNORD', 50),
	('CAB_MAIRIE_PIKNORD', 46),
	('CAB_MAIRIE_PIKNORD', 84),
	('CAB_MAIRIE_PIKNORD', 70),
	('CAB_MAIRIE_PIKNORD', 58),
	('CAB_MAIRIE_PIKNORD', 57),

	('ABPC_Dakar', 97),
	('CAB_MAIRIE_OUAKAM', 97),
	('CAB_MAIRIE_GOLFSUD', 97),
	('ABPC_RF', 97),
	('CAB_MAIRIE_BARGNY', 97),
	('CAB_MAIRIE_YENE', 97),
	('ABPC_Pikine', 97),
	('CAB_MAIRIE_MBAO', 97),
	('CAB_MAIRIE_CAMBERENE', 97),
	('CABMR', 83),
	('CABMR', 71),

	('ABPC_Dakar', 96),
	('ARCVTRESOR', 96),

	('CAB_MAIRIE_RUFISQUE_EST', 83),
	('CAB_MAIRIE_RUFISQUE_EST', 71),
	('CAB_MAIRIE_RUFISQUE_EST', 50),
	('CAB_MAIRIE_RUFISQUE_EST', 46),
	('CAB_MAIRIE_RUFISQUE_EST', 63),
	('CAB_MAIRIE_RUFISQUE_EST', 51),
	('CAB_MAIRIE_RUFISQUE_EST', 47),


	('CAB_MAIRIE_RUFIQUE_OUEST', 83),
	('CAB_MAIRIE_RUFIQUE_OUEST', 71),
	('CAB_MAIRIE_RUFIQUE_OUEST', 50),
	('CAB_MAIRIE_RUFIQUE_OUEST', 46),
	('CAB_MAIRIE_RUFIQUE_OUEST', 63),
	('CAB_MAIRIE_RUFIQUE_OUEST', 51),
	('CAB_MAIRIE_RUFIQUE_OUEST', 47),


	('CAB_MAIRIE_RUFISQUE_NORD', 83),
	('CAB_MAIRIE_RUFISQUE_NORD', 71),
	('CAB_MAIRIE_RUFISQUE_NORD', 50),
	('CAB_MAIRIE_RUFISQUE_NORD', 46),

	('CAB_MAIRIE_SANGALKAM', 83),
	('CAB_MAIRIE_SANGALKAM', 71),
	('CAB_MAIRIE_SANGALKAM', 50),
	('CAB_MAIRIE_SANGALKAM', 46),


	
	('CAB_MAIRIE_BISCUITERIE', 97),
	('CAB_MAIRIE_COLOBANE', 97),
	('CAB_MAIRIE_DALIFORT', 97),
	('CAB_MAIRIE_DIACKSAO', 97),
	('CAB_MAIRIE_DIAMEGUENE', 97),
	('CAB_MAIRIE_DIEUPPEUL', 97),
	('CAB_MAIRIE_THIAROYEKAO', 97),
	('CAB_MAIRIE_FANN', 97),
	('CAB_MAIRIE_GOREE', 97),
	('CAB_MAIRIE_GRANDAKAR', 97),
	('CAB_MAIRIE_GRANDYOFF', 97),
	('CAB_MAIRIE_GUINORD', 97),
	('CAB_MAIRIE_GUISUD', 97),
	('CAB_MAIRIE_HANN', 97),
	('CAB_MAIRIE_HLM', 97),
	('CAB_MAIRIE_KEURMASSAR', 97),
	('CAB_MAIRIE_MALIKA', 97),
	('CAB_MAIRIE_MEDGOUNASS', 97),
	('CAB_MAIRIE_MEDINA', 97),
	('CAB_MAIRIE_LIMAMOULAYE', 97),
	('CAB_MAIRIE_PA', 97),
	('CAB_MAIRIE_PATTEDOIE', 97),
	('CAB_MAIRIE_PIKEST', 97),
	('CAB_MAIRIE_PLATEAU', 97),
	('CAB_MAIRIE_NOTAIRE', 97),
	('CAB_MAIRIE_SICAP', 97),
	('CAB_MAIRIE_THIAMER', 97),
	('CAB_MAIRIE_THIAGARE', 97),
	('CAB_MAIRIE_WAKHINANE', 97),
	('CAB_MAIRIE_YEUMNORD', 97),
	('CAB_MAIRIE_YEUMSUD', 97),
	('CAB_MAIRIE_YOFF', 97),
	('CAB_MAIRIE_MERMOZ', 97),

	('CAB_MAIRIE_JAXAAY', 83),
	('CAB_MAIRIE_JAXAAY', 71),
	('CAB_MAIRIE_JAXAAY', 50),
	('CAB_MAIRIE_JAXAAY', 46),
	('CAB_MAIRIE_JAXAAY', 97),
	('CAB_MAIRIE_JAXAAY', 84),
	('CAB_MAIRIE_JAXAAY', 70),
	('CAB_MAIRIE_JAXAAY', 58),
	('CAB_MAIRIE_JAXAAY', 57),

	('CAB_MAIRIE_SANGALKAM', 69),
	('CAB_MAIRIE_SANGALKAM', 56),
	('CAB_MAIRIE_SANGALKAM', 55),
	('CAB_MAIRIE_SANGALKAM', 54),
	('CAB_MAIRIE_SANGALKAM', 97),

	('CAB_MAIRIE_BAMBYLOR', 97),


	('AG_RECETTE_BISCUITERIE', 96),
	('AG_RECETTE_BISCUITERIE', 97),
	('AG_RECETTE_OUAKAM', 96),
	('AG_RECETTE_OUAKAM', 97),
	('AG_RECETTE_NGOR', 96),
	('AG_RECETTE_NGOR', 97),
	('AG_RECETTE_CAMBERENE', 96),
	('AG_RECETTE_CAMBERENE', 97),
	('AG_RECETTE_YENE', 96),
	('AG_RECETTE_YENE', 97),
	('AG_RECETTE_GRDYOFF', 96),
	('AG_RECETTE_GRDYOFF', 97),
	('AG_RECETTE_HLM', 96),
	('AG_RECETTE_HLM', 97),
	('AG_RECETTE_PLATEAU', 96),
	('AG_RECETTE_PLATEAU', 97),
	('AG_RECETTE_COLOBANE', 96),
	('AG_RECETTE_COLOBANE', 97),
	('AG_RECETTE_DALIFORT', 96),
	('AG_RECETTE_DALIFORT', 97),
	('AG_RECETTE_DIACKSAO', 96),
	('AG_RECETTE_DIACKSAO', 97),
	('AG_RECETTE_DIAMEGUENE', 96),
	('AG_RECETTE_DIAMEGUENE', 97),
	('AG_RECETTE_DIEUPEUL', 96),
	('AG_RECETTE_DIEUPEUL', 97),
	('AG_RECETTE_MERMOZ', 96),
	('AG_RECETTE_MERMOZ', 97),
	('AG_RECETTE_BARGNY', 96),
	('AG_RECETTE_BARGNY', 97),
	('AG_RECETTE_MALIKA', 96),
	('AG_RECETTE_MALIKA', 97),
	('AG_RECETTE_KEUR_MASSAR', 96),
	('AG_RECETTE_KEUR_MASSAR', 97),
	('AG_RECETTE_SICAP_LIBERTE', 96),
	('AG_RECETTE_SICAP_LIBERTE', 97),
	('AG_RECETTE_BAMBYLOR', 96),
	('AG_RECETTE_BAMBYLOR', 97),
	('AG_RECETTE_SANGALKAM', 96),
	('AG_RECETTE_SANGALKAM', 97),
	('AG_RECETTE_NIACOU', 96),
	('AG_RECETTE_NIACOU', 97),
	('AG_RECETTE_YOFF', 96),
	('AG_RECETTE_YOFF', 97),
	('AG_RECETTE_YEUMSUD', 96),
	('AG_RECETTE_YEUMSUD', 97),
	('AG_RECETTE_YEUMNORD', 96),
	('AG_RECETTE_YEUMNORD', 97),
	('AG_RECETTE_WAKHINANE', 96),
	('AG_RECETTE_WAKHINANE', 97),
	('AG_RECETTE_THIAROYE_GARE', 96),
	('AG_RECETTE_THIAROYE_GARE', 97),
	('AG_RECETTE_THIAROYE_MER', 96),
	('AG_RECETTE_THIAROYE_MER', 97),
	('AG_RECETTE_SAM_NOTAIRE', 96),
	('AG_RECETTE_SAM_NOTAIRE', 97),
	('AG_RECETTE_PIKINE_EST', 96),
	('AG_RECETTE_PIKINE_EST', 97),
	('AG_RECETTE_PATTEDOIE', 96),
	('AG_RECETTE_PATTEDOIE', 97),
	('AG_RECETTE_PARCELLES', 96),
	('AG_RECETTE_PARCELLES', 97),
	('AG_RECETTE_NDIAREME_LIMAMOULAYE', 96),
	('AG_RECETTE_NDIAREME_LIMAMOULAYE', 97),
	('AG_RECETTE_MEDINA', 96),
	('AG_RECETTE_MEDINA', 97),
	('AG_RECETTE_MEDINA_GOUNASS', 97),
	('AG_RECETTE_MEDINA_GOUNASS', 96),
	('AG_RECETTE_MBAO', 96),
	('AG_RECETTE_MBAO', 97),
	('AG_RECETTE_HANN_BELAIR', 96),
	('AG_RECETTE_HANN_BELAIR', 97),
	('AG_RECETTE_GUINAW_RAIL_SUD', 96),
	('AG_RECETTE_GUINAW_RAIL_SUD', 97),
	('AG_RECETTE_GUINAW_RAIL_NORD', 96),
	('AG_RECETTE_GUINAW_RAIL_NORD', 97),
	('AG_RECETTE_GRAND_DAKAR', 96),
	('AG_RECETTE_GRAND_DAKAR', 97),
	('AG_RECETTE_GOREE', 96),
	('AG_RECETTE_GOREE', 97),
	('AG_RECETTE_GOLF_SUD', 96),
	('AG_RECETTE_GOLF_SUD', 97),
	('AG_RECETTE_FANN_POINT_E_AMITIE', 96),
	('AG_RECETTE_FANN_POINT_E_AMITIE', 97),
	('AG_RECETTE_DJIDAH_THIAROYE_KAO', 96),
	('AG_RECETTE_DJIDAH_THIAROYE_KAO', 97),
	('ABPC_Pikine', 96),

	('CAB_MAIRIE_TIVAOUNE_PEULH', 97),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 83),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 71),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 50),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 46),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 84),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 70),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 58),
	('CAB_MAIRIE_TIVAOUNE_PEULH', 57),

	('AG_RECETTE_TIV_PEULH_NIAGUE', 96),
	('AG_RECETTE_TIV_PEULH_NIAGUE', 97),

	('CAB_MAIRIE_SENDOU', 83),
	('CAB_MAIRIE_SENDOU', 71),
	('CAB_MAIRIE_SENDOU', 50),
	('CAB_MAIRIE_SENDOU', 46),
	('CAB_MAIRIE_SENDOU', 84),
	('CAB_MAIRIE_SENDOU', 70),
	('CAB_MAIRIE_SENDOU', 58),
	('CAB_MAIRIE_SENDOU', 57),

	('CAB_MAIRIE_RUFISQUE_NORD', 97),
	('CAB_MAIRIE_RUFISQUE_NORD', 84),
	('CAB_MAIRIE_RUFISQUE_NORD', 70),
	('CAB_MAIRIE_RUFISQUE_NORD', 58),
	('CAB_MAIRIE_RUFISQUE_NORD', 57),

	('CAB_MAIRIE_RUFISK_EST', 83),
	('CAB_MAIRIE_RUFISK_EST', 71),
	('CAB_MAIRIE_RUFISK_EST', 50),
	('CAB_MAIRIE_RUFISK_EST', 46),

	('CAB_MAIRIE_RUFISK_EST', 84),
	('CAB_MAIRIE_RUFISK_EST', 70),
	('CAB_MAIRIE_RUFISK_EST', 58),
	('CAB_MAIRIE_RUFISK_EST', 57),
	('CAB_MAIRIE_RUFISK_EST', 97),

	('CAB_MAIRIE_RUFIQUE_OUEST', 97),

	('CAB_MAIRIE_DIAMNIADIO', 97),
	('AG_RECETTE_DIAMNIADIO', 96),
	('AG_RECETTE_DIAMNIADIO', 97),
	('CAB_MAIRIE_DIAMNIADIO', 83),
	('CAB_MAIRIE_DIAMNIADIO', 71),
	('CAB_MAIRIE_DIAMNIADIO', 50),
	('CAB_MAIRIE_DIAMNIADIO', 46),
	('CAB_MAIRIE_DIAMNIADIO', 84),
	('CAB_MAIRIE_DIAMNIADIO', 70),
	('CAB_MAIRIE_DIAMNIADIO', 58),
	('CAB_MAIRIE_DIAMNIADIO', 57),

	('CAB_MAIRIE_SENDOU', 97),

	('CAB_MAIRIE_SEBIKOTANE', 83),
	('CAB_MAIRIE_SEBIKOTANE', 71),
	('CAB_MAIRIE_SEBIKOTANE', 50),
	('CAB_MAIRIE_SEBIKOTANE', 46),
	('CAB_MAIRIE_SEBIKOTANE', 84),
	('CAB_MAIRIE_SEBIKOTANE', 70),
	('CAB_MAIRIE_SEBIKOTANE', 58),
	('CAB_MAIRIE_SEBIKOTANE', 57),
	('CAB_MAIRIE_SEBIKOTANE', 97),
	('AG_RECETTE_SEBIKOTANE', 96),
	('AG_RECETTE_SEBIKOTANE', 97),

	('AG_RECETTE_RUFOUEST', 96),
	('AG_RECETTE_RUFOUEST', 97),

	('AG_RECETTE_RUFEST', 96),
	('AG_RECETTE_RUFEST', 97),

	('AG_RECETTE_RUFNORD', 96),
	('AG_RECETTE_RUFNORD', 97),
	
	('AG_RECETTE_SENDOU', 96),
	('AG_RECETTE_SENDOU', 97),

	('ABPC_Dakar', 85),
	('ABPC_Dakar', 86),
	('ABPC_Dakar', 87),
	('ABPC_Dakar', 88),
	('ABPC_Dakar', 79),



	('ABPC_Pikine', 85),
	('ABPC_Pikine', 86),
	('ABPC_Pikine', 87),
	('ABPC_Pikine', 88),
	('ABPC_Pikine', 79),

	('ABPC_RF', 85),
	('ABPC_RF', 86),
	('ABPC_RF', 87),
	('ABPC_RF', 88),
	('ABPC_RF', 79),


	
	('ABD_Dakar', 100),
	('ABD_RF', 100),
	('CSDU_RF', 101),
	('ABD_GW', 100),
	('CSDU_GW', 101),
	('ABD_Pikine', 100),
	('CSDUP', 101),

	('ABPC_RF', 96),



	('CSRUD', 114),
	('CSRUD', 113),
	('CSRUD', 112),
	('CSRUD', 107),
	('CSRUD', 110),
	('CSRUD', 109),
	('CSRUD', 108),
	('CSRUD', 106),
	('CSRUD', 105),
	('CSRUD', 102),
	('CSRUD', 117),
	('CSRUD', 118),


	('FCT_SP_PKG', 40),
	('FCT_SP_PKG', 61),
	('FCT_SP_PKG', 74),
	('FCT_SP_PKG', 81),

	('FCT_SP_PKG', 62),
	('FCT_SP_PKG', 59),
	

	('CSDU_RF', 102),
	('CSDU_RF', 105),
	('CSDU_RF', 106),
	('CSDU_RF', 107),
	
	('CSDUP', 102),
	('CSDUP', 105),
	('CSDUP', 106),
	('CSDUP', 108),
	('CSDUP', 110),
	('CSDUP', 109),
	('CSDUP', 117),
	('CSDUP', 118),
	('CSDUP', 114),
	('CSDUP', 113),
	('CSDUP', 112),
	('CSDUP', 107),
	
	('CSDU_GW', 102),
	('CSDU_GW', 105),
	('CSDU_GW', 117),
	('CSDU_GW', 110),
	('CSDU_GW', 109),
	('CSDU_GW', 108),
	('CSDU_GW', 106),
	('CSDU_GW', 118),
	('CSDU_GW', 114),
	('CSDU_GW', 113),
	('CSDU_GW', 112),
	('CSDU_GW', 107),



	('CSDU_RF', 108),
	('CSDU_RF', 109),
	('CSDU_RF', 117),
	('CSDU_RF', 110),
	('CSDU_RF', 118),
	('CSDU_RF', 114),
	('CSDU_RF', 113),
	('CSDU_RF', 112),

	('ADOM_DakarPlateau', 96),
	('ADOM_DakarPlateau', 97),
	('ADOM_Ngor_Almadie_GRDK', 96),
	('ADOM_Ngor_Almadie_GRDK', 97),

	('AD_RF', 96),
	('AD_RF', 97),
	('ADOM_PG', 96),
	('ADOM_PG', 97),



	('CONS_GD', 96),
	('CONS_GD', 97),
	('CONS_NA', 96),
	('CONS_NA', 97);

/*

	(14, 'SOUS_PREF_BARGNY', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_BARGNY', 'VIEW'),
	(14, 'SOUS_PREF_BARGNY', 'RELOAD'),
	(14, 'SOUS_PREF_BARGNY', 'DOWNLOAD'),

		(14, 'SOUS_PREF_RUFISQUE_EST', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_RUFISQUE_EST', 'VIEW'),
	(14, 'SOUS_PREF_RUFISQUE_EST', 'RELOAD'),
	(14, 'SOUS_PREF_RUFISQUE_EST', 'DOWNLOAD'),

*/


INSERT INTO `fonctions_permissions_actes` (`FPA_PIE_ID`, `FPA_FCT_CODE`, `FPA_PA_CODE`) VALUES

	(14, 'SOUS_PREF_ALMADIES', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_ALMADIES', 'VIEW'),
	(14, 'SOUS_PREF_ALMADIES', 'RELOAD'),
	(14, 'SOUS_PREF_ALMADIES', 'DOWNLOAD'),

	(14, 'SOUS_PREF_BAMBYLOR', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_BAMBYLOR', 'VIEW'),
        (14, 'SOUS_PREF_BAMBYLOR', 'RELOAD'),
	(14, 'SOUS_PREF_BAMBYLOR', 'DOWNLOAD'),

	(14, 'SOUS_PREF_DAKPLATEAU', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_DAKPLATEAU', 'VIEW'),
	(14, 'SOUS_PREF_DAKPLATEAU', 'RELOAD'),
	(14, 'SOUS_PREF_DAKPLATEAU', 'DOWNLOAD'),

	(14, 'SOUS_PREF_NIAYES', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_NIAYES', 'VIEW'),
	(14, 'SOUS_PREF_NIAYES', 'RELOAD'),
	(14, 'SOUS_PREF_NIAYES', 'DOWNLOAD'),

	(14, 'SOUS_PREF_PA', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_PA', 'VIEW'),
	(14, 'SOUS_PREF_PA', 'RELOAD'),
	(14, 'SOUS_PREF_PA', 'DOWNLOAD'),

	(14, 'SOUS_PREF_DAGOUDANE', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_DAGOUDANE', 'VIEW'),
	(14, 'SOUS_PREF_DAGOUDANE', 'RELOAD'),
	(14, 'SOUS_PREF_DAGOUDANE', 'DOWNLOAD'),

	(14, 'SOUS_PREF_THIAROYE', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_THIAROYE', 'VIEW'),
	(14, 'SOUS_PREF_THIAROYE', 'RELOAD'),
	(14, 'SOUS_PREF_THIAROYE', 'DOWNLOAD'),

	(14, 'SOUS_PREF_GRDAK', 'VIEW_HISTORY'),
	(14, 'SOUS_PREF_GRDAK', 'VIEW'),
	(14, 'SOUS_PREF_GRDAK', 'RELOAD'),
	(14, 'SOUS_PREF_GRDAK', 'DOWNLOAD'),


	(14, 'SOUS_PREF_GUEDIAWAYE', 'DOWNLOAD'),
	(14, 'SOUS_PREF_GUEDIAWAYE', 'RELOAD'),
	(14, 'SOUS_PREF_GUEDIAWAYE', 'VIEW'),
	(14, 'SOUS_PREF_GUEDIAWAYE', 'VIEW_HISTORY'),


	(14, 'PREFET_RUFISQUE', 'VIEW_HISTORY'),
	(14, 'PREFET_RUFISQUE', 'VIEW'),
	(14, 'PREFET_RUFISQUE', 'RELOAD'),
	(14, 'PREFET_RUFISQUE', 'DOWNLOAD'),

	(14, 'SOUSPREF_RUFISQUEEST ', 'VIEW_HISTORY'),
	(14, 'SOUSPREF_RUFISQUEEST ', 'VIEW'),
	(14, 'SOUSPREF_RUFISQUEEST ', 'RELOAD'),
	(14, 'SOUSPREF_RUFISQUEEST ', 'DOWNLOAD'),
	


	(19, 'SOUS_PREF_ALMADIES', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_ALMADIES', 'VIEW'),
	(19, 'SOUS_PREF_ALMADIES', 'RELOAD'),
	(19, 'SOUS_PREF_ALMADIES', 'DOWNLOAD'),

	(19, 'SOUS_PREF_BAMBYLOR', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_BAMBYLOR', 'VIEW'),
    (19, 'SOUS_PREF_BAMBYLOR', 'RELOAD'),
	(19, 'SOUS_PREF_BAMBYLOR', 'DOWNLOAD'),

	(19, 'SOUS_PREF_DAKPLATEAU', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_DAKPLATEAU', 'VIEW'),
	(19, 'SOUS_PREF_DAKPLATEAU', 'RELOAD'),
	(19, 'SOUS_PREF_DAKPLATEAU', 'DOWNLOAD'),

	(19, 'SOUS_PREF_NIAYES', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_NIAYES', 'VIEW'),
	(19, 'SOUS_PREF_NIAYES', 'RELOAD'),
	(19, 'SOUS_PREF_NIAYES', 'DOWNLOAD'),

	(19, 'SOUS_PREF_PA', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_PA', 'VIEW'),
	(19, 'SOUS_PREF_PA', 'RELOAD'),
	(19, 'SOUS_PREF_PA', 'DOWNLOAD'),

	(19, 'SOUS_PREF_DAGOUDANE', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_DAGOUDANE', 'VIEW'),
	(19, 'SOUS_PREF_DAGOUDANE', 'RELOAD'),
	(19, 'SOUS_PREF_DAGOUDANE', 'DOWNLOAD'),

	(19, 'SOUS_PREF_THIAROYE', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_THIAROYE', 'VIEW'),
	(19, 'SOUS_PREF_THIAROYE', 'RELOAD'),
	(19, 'SOUS_PREF_THIAROYE', 'DOWNLOAD'),

	(19, 'SOUS_PREF_GRDAK', 'VIEW_HISTORY'),
	(19, 'SOUS_PREF_GRDAK', 'VIEW'),
	(19, 'SOUS_PREF_GRDAK', 'RELOAD'),
	(19, 'SOUS_PREF_GRDAK', 'DOWNLOAD'),


	(19, 'SOUS_PREF_GUEDIAWAYE', 'DOWNLOAD'),
	(19, 'SOUS_PREF_GUEDIAWAYE', 'RELOAD'),
	(19, 'SOUS_PREF_GUEDIAWAYE', 'VIEW'),
	(19, 'SOUS_PREF_GUEDIAWAYE', 'VIEW_HISTORY'),


	(19, 'PREFET_RUFISQUE', 'VIEW_HISTORY'),
	(19, 'PREFET_RUFISQUE', 'VIEW'),
	(19, 'PREFET_RUFISQUE', 'RELOAD'),
	(19, 'PREFET_RUFISQUE', 'DOWNLOAD'),

	(19, 'SOUSPREF_RUFISQUEEST ', 'VIEW_HISTORY'),
	(19, 'SOUSPREF_RUFISQUEEST ', 'VIEW'),
	(19, 'SOUSPREF_RUFISQUEEST ', 'RELOAD'),
	(19, 'SOUSPREF_RUFISQUEEST ', 'DOWNLOAD'),


	(10, 'CAB_MAIRIE_NGOR', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_NGOR', 'VIEW'),
	(10, 'CAB_MAIRIE_NGOR', 'RELOAD'),
	(10, 'CAB_MAIRIE_NGOR', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_BISCUITERIE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_BISCUITERIE', 'VIEW'),
	(10, 'CAB_MAIRIE_BISCUITERIE', 'RELOAD'),
	(10, 'CAB_MAIRIE_BISCUITERIE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_CAMBERENE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_CAMBERENE', 'VIEW'),
	(10, 'CAB_MAIRIE_CAMBERENE', 'RELOAD'),
	(10, 'CAB_MAIRIE_CAMBERENE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_COLOBANE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_COLOBANE', 'VIEW'),
	(10, 'CAB_MAIRIE_COLOBANE', 'RELOAD'),
	(10, 'CAB_MAIRIE_COLOBANE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_DALIFORT', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_DALIFORT', 'VIEW'),
	(10, 'CAB_MAIRIE_DALIFORT', 'RELOAD'),
	(10, 'CAB_MAIRIE_DALIFORT', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_DIACKSAO', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_DIACKSAO', 'VIEW'),
	(10, 'CAB_MAIRIE_DIACKSAO', 'RELOAD'),
	(10, 'CAB_MAIRIE_DIACKSAO', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW'),
	(10, 'CAB_MAIRIE_DIAMEGUENE', 'RELOAD'),
	(10, 'CAB_MAIRIE_DIAMEGUENE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW'),
	(10, 'CAB_MAIRIE_DIEUPPEUL', 'RELOAD'),
	(10, 'CAB_MAIRIE_DIEUPPEUL', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW'),
	(10, 'CAB_MAIRIE_THIAROYEKAO', 'RELOAD'),
	(10, 'CAB_MAIRIE_THIAROYEKAO', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_FANN', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_FANN', 'VIEW'),
	(10, 'CAB_MAIRIE_FANN', 'RELOAD'),
	(10, 'CAB_MAIRIE_FANN', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_GOLFSUD', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_GOLFSUD', 'VIEW'),
	(10, 'CAB_MAIRIE_GOLFSUD', 'RELOAD'),
	(10, 'CAB_MAIRIE_GOLFSUD', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_GOREE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_GOREE', 'VIEW'),
	(10, 'CAB_MAIRIE_GOREE', 'RELOAD'),
	(10, 'CAB_MAIRIE_GOREE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_GRANDAKAR', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_GRANDAKAR', 'VIEW'),
	(10, 'CAB_MAIRIE_GRANDAKAR', 'RELOAD'),
	(10, 'CAB_MAIRIE_GRANDAKAR', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_GRANDYOFF', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_GRANDYOFF', 'VIEW'),
	(10, 'CAB_MAIRIE_GRANDYOFF', 'RELOAD'),
	(10, 'CAB_MAIRIE_GRANDYOFF', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_GUINORD', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_GUINORD', 'VIEW'),
	(10, 'CAB_MAIRIE_GUINORD', 'RELOAD'),
	(10, 'CAB_MAIRIE_GUINORD', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_GUISUD', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_GUISUD', 'VIEW'),
	(10, 'CAB_MAIRIE_GUISUD', 'RELOAD'),
	(10, 'CAB_MAIRIE_GUISUD', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_HANN', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_HANN', 'VIEW'),
	(10, 'CAB_MAIRIE_HANN', 'RELOAD'),
	(10, 'CAB_MAIRIE_HANN', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_HLM', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_HLM', 'VIEW'),
	(10, 'CAB_MAIRIE_HLM', 'RELOAD'),
	(10, 'CAB_MAIRIE_HLM', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_KEURMASSAR', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_KEURMASSAR', 'VIEW'),
	(10, 'CAB_MAIRIE_KEURMASSAR', 'RELOAD'),
	(10, 'CAB_MAIRIE_KEURMASSAR', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_MALIKA', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_MALIKA', 'VIEW'),
	(10, 'CAB_MAIRIE_MALIKA', 'RELOAD'),
	(10, 'CAB_MAIRIE_MALIKA', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_MBAO', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_MBAO', 'VIEW'),
	(10, 'CAB_MAIRIE_MBAO', 'RELOAD'),
	(10, 'CAB_MAIRIE_MBAO', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW'),
	(10, 'CAB_MAIRIE_MEDGOUNASS', 'RELOAD'),
	(10, 'CAB_MAIRIE_MEDGOUNASS', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_MEDINA', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_MEDINA', 'VIEW'),
	(10, 'CAB_MAIRIE_MEDINA', 'RELOAD'),
	(10, 'CAB_MAIRIE_MEDINA', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW'),
	(10, 'CAB_MAIRIE_LIMAMOULAYE', 'RELOAD'),
	(10, 'CAB_MAIRIE_LIMAMOULAYE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_OUAKAM', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_OUAKAM', 'VIEW'),
	(10, 'CAB_MAIRIE_OUAKAM', 'RELOAD'),
	(10, 'CAB_MAIRIE_OUAKAM', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_PA', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_PA', 'VIEW'),
	(10, 'CAB_MAIRIE_PA', 'RELOAD'),
	(10, 'CAB_MAIRIE_PA', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_PATTEDOIE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_PATTEDOIE', 'VIEW'),
	(10, 'CAB_MAIRIE_PATTEDOIE', 'RELOAD'),
	(10, 'CAB_MAIRIE_PATTEDOIE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_PIKEST', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_PIKEST', 'VIEW'),
	(10, 'CAB_MAIRIE_PIKEST', 'RELOAD'),
	(10, 'CAB_MAIRIE_PIKEST', 'DOWNLOAD'),

        (10, 'CAB_MAIRIE_PIKOUEST', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_PIKOUEST', 'VIEW'),
	(10, 'CAB_MAIRIE_PIKOUEST', 'RELOAD'),
	(10, 'CAB_MAIRIE_PIKOUEST', 'DOWNLOAD'),

        (10, 'CAB_MAIRIE_PIKNORD', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_PIKNORD', 'VIEW'),
	(10, 'CAB_MAIRIE_PIKNORD', 'RELOAD'),
	(10, 'CAB_MAIRIE_PIKNORD', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_PLATEAU', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_PLATEAU', 'VIEW'),
	(10, 'CAB_MAIRIE_PLATEAU', 'RELOAD'),
	(10, 'CAB_MAIRIE_PLATEAU', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_NOTAIRE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_NOTAIRE', 'VIEW'),
	(10, 'CAB_MAIRIE_NOTAIRE', 'RELOAD'),
	(10, 'CAB_MAIRIE_NOTAIRE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_SICAP', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_SICAP', 'VIEW'),
	(10, 'CAB_MAIRIE_SICAP', 'RELOAD'),
	(10, 'CAB_MAIRIE_SICAP', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_THIAMER', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_THIAMER', 'VIEW'),
	(10, 'CAB_MAIRIE_THIAMER', 'RELOAD'),
	(10, 'CAB_MAIRIE_THIAMER', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_THIAGARE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_THIAGARE', 'VIEW'),
	(10, 'CAB_MAIRIE_THIAGARE', 'RELOAD'),
	(10, 'CAB_MAIRIE_THIAGARE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_WAKHINANE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_WAKHINANE', 'VIEW'),
	(10, 'CAB_MAIRIE_WAKHINANE', 'RELOAD'),
	(10, 'CAB_MAIRIE_WAKHINANE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_YEUMNORD', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_YEUMNORD', 'VIEW'),
	(10, 'CAB_MAIRIE_YEUMNORD', 'RELOAD'),
	(10, 'CAB_MAIRIE_YEUMNORD', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_YEUMSUD', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_YEUMSUD', 'VIEW'),
	(10, 'CAB_MAIRIE_YEUMSUD', 'RELOAD'),
	(10, 'CAB_MAIRIE_YEUMSUD', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_YOFF', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_YOFF', 'VIEW'),
	(10, 'CAB_MAIRIE_YOFF', 'RELOAD'),
	(10, 'CAB_MAIRIE_YOFF', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_MERMOZ', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_MERMOZ', 'VIEW'),
	(10, 'CAB_MAIRIE_MERMOZ', 'RELOAD'),
	(10, 'CAB_MAIRIE_MERMOZ', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_BARGNY', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_BARGNY', 'VIEW'),
	(10, 'CAB_MAIRIE_BARGNY', 'RELOAD'),
	(10, 'CAB_MAIRIE_BARGNY', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_BAMBYLOR', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_BAMBYLOR', 'VIEW'),
	(10, 'CAB_MAIRIE_BAMBYLOR', 'RELOAD'),
	(10, 'CAB_MAIRIE_BAMBYLOR', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_YENE', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_YENE', 'VIEW'),
	(10, 'CAB_MAIRIE_YENE', 'RELOAD'),
	(10, 'CAB_MAIRIE_YENE', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_SENDOU', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_SENDOU', 'VIEW'),
	(10, 'CAB_MAIRIE_SENDOU', 'RELOAD'),
	(10, 'CAB_MAIRIE_SENDOU', 'DOWNLOAD'),

	(10, 'CAB_MAIRIE_SANGALKAM', 'VIEW_HISTORY'),
	(10, 'CAB_MAIRIE_SANGALKAM', 'VIEW'),
	(10, 'CAB_MAIRIE_SANGALKAM', 'RELOAD'),
	(10, 'CAB_MAIRIE_SANGALKAM', 'DOWNLOAD'),


	(18, 'CAB_MAIRIE_NGOR', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_NGOR', 'VIEW'),
	(18, 'CAB_MAIRIE_NGOR', 'RELOAD'),
	(18, 'CAB_MAIRIE_NGOR', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_BISCUITERIE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_BISCUITERIE', 'VIEW'),
	(18, 'CAB_MAIRIE_BISCUITERIE', 'RELOAD'),
	(18, 'CAB_MAIRIE_BISCUITERIE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_CAMBERENE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_CAMBERENE', 'VIEW'),
	(18, 'CAB_MAIRIE_CAMBERENE', 'RELOAD'),
	(18, 'CAB_MAIRIE_CAMBERENE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_COLOBANE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_COLOBANE', 'VIEW'),
	(18, 'CAB_MAIRIE_COLOBANE', 'RELOAD'),
	(18, 'CAB_MAIRIE_COLOBANE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_DALIFORT', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_DALIFORT', 'VIEW'),
	(18, 'CAB_MAIRIE_DALIFORT', 'RELOAD'),
	(18, 'CAB_MAIRIE_DALIFORT', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_DIACKSAO', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_DIACKSAO', 'VIEW'),
	(18, 'CAB_MAIRIE_DIACKSAO', 'RELOAD'),
	(18, 'CAB_MAIRIE_DIACKSAO', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW'),
	(18, 'CAB_MAIRIE_DIAMEGUENE', 'RELOAD'),
	(18, 'CAB_MAIRIE_DIAMEGUENE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW'),
	(18, 'CAB_MAIRIE_DIEUPPEUL', 'RELOAD'),
	(18, 'CAB_MAIRIE_DIEUPPEUL', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW'),
	(18, 'CAB_MAIRIE_THIAROYEKAO', 'RELOAD'),
	(18, 'CAB_MAIRIE_THIAROYEKAO', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_FANN', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_FANN', 'VIEW'),
	(18, 'CAB_MAIRIE_FANN', 'RELOAD'),
	(18, 'CAB_MAIRIE_FANN', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_GOLFSUD', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_GOLFSUD', 'VIEW'),
	(18, 'CAB_MAIRIE_GOLFSUD', 'RELOAD'),
	(18, 'CAB_MAIRIE_GOLFSUD', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_GOREE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_GOREE', 'VIEW'),
	(18, 'CAB_MAIRIE_GOREE', 'RELOAD'),
	(18, 'CAB_MAIRIE_GOREE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_GRANDAKAR', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_GRANDAKAR', 'VIEW'),
	(18, 'CAB_MAIRIE_GRANDAKAR', 'RELOAD'),
	(18, 'CAB_MAIRIE_GRANDAKAR', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_GRANDYOFF', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_GRANDYOFF', 'VIEW'),
	(18, 'CAB_MAIRIE_GRANDYOFF', 'RELOAD'),
	(18, 'CAB_MAIRIE_GRANDYOFF', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_GUINORD', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_GUINORD', 'VIEW'),
	(18, 'CAB_MAIRIE_GUINORD', 'RELOAD'),
	(18, 'CAB_MAIRIE_GUINORD', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_GUISUD', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_GUISUD', 'VIEW'),
	(18, 'CAB_MAIRIE_GUISUD', 'RELOAD'),
	(18, 'CAB_MAIRIE_GUISUD', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_HANN', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_HANN', 'VIEW'),
	(18, 'CAB_MAIRIE_HANN', 'RELOAD'),
	(18, 'CAB_MAIRIE_HANN', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_HLM', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_HLM', 'VIEW'),
	(18, 'CAB_MAIRIE_HLM', 'RELOAD'),
	(18, 'CAB_MAIRIE_HLM', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_KEURMASSAR', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_KEURMASSAR', 'VIEW'),
	(18, 'CAB_MAIRIE_KEURMASSAR', 'RELOAD'),
	(18, 'CAB_MAIRIE_KEURMASSAR', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_MALIKA', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_MALIKA', 'VIEW'),
	(18, 'CAB_MAIRIE_MALIKA', 'RELOAD'),
	(18, 'CAB_MAIRIE_MALIKA', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_MBAO', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_MBAO', 'VIEW'),
	(18, 'CAB_MAIRIE_MBAO', 'RELOAD'),
	(18, 'CAB_MAIRIE_MBAO', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW'),
	(18, 'CAB_MAIRIE_MEDGOUNASS', 'RELOAD'),
	(18, 'CAB_MAIRIE_MEDGOUNASS', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_MEDINA', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_MEDINA', 'VIEW'),
	(18, 'CAB_MAIRIE_MEDINA', 'RELOAD'),
	(18, 'CAB_MAIRIE_MEDINA', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW'),
	(18, 'CAB_MAIRIE_LIMAMOULAYE', 'RELOAD'),
	(18, 'CAB_MAIRIE_LIMAMOULAYE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_OUAKAM', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_OUAKAM', 'VIEW'),
	(18, 'CAB_MAIRIE_OUAKAM', 'RELOAD'),
	(18, 'CAB_MAIRIE_OUAKAM', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_PA', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_PA', 'VIEW'),
	(18, 'CAB_MAIRIE_PA', 'RELOAD'),
	(18, 'CAB_MAIRIE_PA', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_PATTEDOIE', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_PATTEDOIE', 'VIEW'),
	(18, 'CAB_MAIRIE_PATTEDOIE', 'RELOAD'),
	(18, 'CAB_MAIRIE_PATTEDOIE', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_PIKEST', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_PIKEST', 'VIEW'),
	(18, 'CAB_MAIRIE_PIKEST', 'RELOAD'),
	(18, 'CAB_MAIRIE_PIKEST', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_PIKOUEST', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_PIKOUEST', 'VIEW'),
	(18, 'CAB_MAIRIE_PIKOUEST', 'RELOAD'),
	(18, 'CAB_MAIRIE_PIKOUEST', 'DOWNLOAD'),

	(18, 'CAB_MAIRIE_PIKNORD', 'VIEW_HISTORY'),
	(18, 'CAB_MAIRIE_PIKNORD', 'VIEW'),
	(18, 'CAB_MAIRIE_PIKNORD', 'RELOAD'),
	(18, 'CAB_MAIRIE_PIKNORD', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_PLATEAU', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_PLATEAU', 'VIEW'),
		(18, 'CAB_MAIRIE_PLATEAU', 'RELOAD'),
		(18, 'CAB_MAIRIE_PLATEAU', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_NOTAIRE', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_NOTAIRE', 'VIEW'),
		(18, 'CAB_MAIRIE_NOTAIRE', 'RELOAD'),
		(18, 'CAB_MAIRIE_NOTAIRE', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_SICAP', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_SICAP', 'VIEW'),
		(18, 'CAB_MAIRIE_SICAP', 'RELOAD'),
		(18, 'CAB_MAIRIE_SICAP', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_THIAMER', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_THIAMER', 'VIEW'),
		(18, 'CAB_MAIRIE_THIAMER', 'RELOAD'),
		(18, 'CAB_MAIRIE_THIAMER', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_THIAGARE', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_THIAGARE', 'VIEW'),
		(18, 'CAB_MAIRIE_THIAGARE', 'RELOAD'),
		(18, 'CAB_MAIRIE_THIAGARE', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_WAKHINANE', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_WAKHINANE', 'VIEW'),
		(18, 'CAB_MAIRIE_WAKHINANE', 'RELOAD'),
		(18, 'CAB_MAIRIE_WAKHINANE', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_YEUMNORD', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_YEUMNORD', 'VIEW'),
		(18, 'CAB_MAIRIE_YEUMNORD', 'RELOAD'),
		(18, 'CAB_MAIRIE_YEUMNORD', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_YEUMSUD', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_YEUMSUD', 'VIEW'),
		(18, 'CAB_MAIRIE_YEUMSUD', 'RELOAD'),
		(18, 'CAB_MAIRIE_YEUMSUD', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_YOFF', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_YOFF', 'VIEW'),
		(18, 'CAB_MAIRIE_YOFF', 'RELOAD'),
		(18, 'CAB_MAIRIE_YOFF', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_MERMOZ', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_MERMOZ', 'VIEW'),
		(18, 'CAB_MAIRIE_MERMOZ', 'RELOAD'),
		(18, 'CAB_MAIRIE_MERMOZ', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_BARGNY', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_BARGNY', 'VIEW'),
		(18, 'CAB_MAIRIE_BARGNY', 'RELOAD'),
		(18, 'CAB_MAIRIE_BARGNY', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_BAMBYLOR', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_BAMBYLOR', 'VIEW'),
		(18, 'CAB_MAIRIE_BAMBYLOR', 'RELOAD'),
		(18, 'CAB_MAIRIE_BAMBYLOR', 'DOWNLOAD'),

		(18, 'CAB_MAIRIE_YENE', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_YENE', 'VIEW'),
		(18, 'CAB_MAIRIE_YENE', 'RELOAD'),
		(18, 'CAB_MAIRIE_YENE', 'DOWNLOAD'),


		(18, 'CAB_MAIRIE_SENDOU', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_SENDOU', 'VIEW'),
		(18, 'CAB_MAIRIE_SENDOU', 'RELOAD'),
		(18, 'CAB_MAIRIE_SENDOU', 'DOWNLOAD'),


		(18, 'CAB_MAIRIE_SANGALKAM', 'VIEW_HISTORY'),
		(18, 'CAB_MAIRIE_SANGALKAM', 'VIEW'),
		(18, 'CAB_MAIRIE_SANGALKAM', 'RELOAD'),
		(18, 'CAB_MAIRIE_SANGALKAM', 'DOWNLOAD'),


	(26, 'CAB_MAIRIE_NGOR', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_NGOR', 'VIEW'),
	(26, 'CAB_MAIRIE_NGOR', 'RELOAD'),
	(26, 'CAB_MAIRIE_NGOR', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_BISCUITERIE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_BISCUITERIE', 'VIEW'),
	(26, 'CAB_MAIRIE_BISCUITERIE', 'RELOAD'),
	(26, 'CAB_MAIRIE_BISCUITERIE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_CAMBERENE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_CAMBERENE', 'VIEW'),
	(26, 'CAB_MAIRIE_CAMBERENE', 'RELOAD'),
	(26, 'CAB_MAIRIE_CAMBERENE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_COLOBANE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_COLOBANE', 'VIEW'),
	(26, 'CAB_MAIRIE_COLOBANE', 'RELOAD'),
	(26, 'CAB_MAIRIE_COLOBANE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_DALIFORT', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_DALIFORT', 'VIEW'),
	(26, 'CAB_MAIRIE_DALIFORT', 'RELOAD'),
	(26, 'CAB_MAIRIE_DALIFORT', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_DIACKSAO', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_DIACKSAO', 'VIEW'),
	(26, 'CAB_MAIRIE_DIACKSAO', 'RELOAD'),
	(26, 'CAB_MAIRIE_DIACKSAO', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW'),
	(26, 'CAB_MAIRIE_DIAMEGUENE', 'RELOAD'),
	(26, 'CAB_MAIRIE_DIAMEGUENE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW'),
	(26, 'CAB_MAIRIE_DIEUPPEUL', 'RELOAD'),
	(26, 'CAB_MAIRIE_DIEUPPEUL', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW'),
	(26, 'CAB_MAIRIE_THIAROYEKAO', 'RELOAD'),
	(26, 'CAB_MAIRIE_THIAROYEKAO', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_FANN', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_FANN', 'VIEW'),
	(26, 'CAB_MAIRIE_FANN', 'RELOAD'),
	(26, 'CAB_MAIRIE_FANN', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_GOLFSUD', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_GOLFSUD', 'VIEW'),
	(26, 'CAB_MAIRIE_GOLFSUD', 'RELOAD'),
	(26, 'CAB_MAIRIE_GOLFSUD', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_GOREE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_GOREE', 'VIEW'),
	(26, 'CAB_MAIRIE_GOREE', 'RELOAD'),
	(26, 'CAB_MAIRIE_GOREE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_GRANDAKAR', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_GRANDAKAR', 'VIEW'),
	(26, 'CAB_MAIRIE_GRANDAKAR', 'RELOAD'),
	(26, 'CAB_MAIRIE_GRANDAKAR', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_GRANDYOFF', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_GRANDYOFF', 'VIEW'),
	(26, 'CAB_MAIRIE_GRANDYOFF', 'RELOAD'),
	(26, 'CAB_MAIRIE_GRANDYOFF', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_GUINORD', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_GUINORD', 'VIEW'),
	(26, 'CAB_MAIRIE_GUINORD', 'RELOAD'),
	(26, 'CAB_MAIRIE_GUINORD', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_GUISUD', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_GUISUD', 'VIEW'),
	(26, 'CAB_MAIRIE_GUISUD', 'RELOAD'),
	(26, 'CAB_MAIRIE_GUISUD', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_HANN', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_HANN', 'VIEW'),
	(26, 'CAB_MAIRIE_HANN', 'RELOAD'),
	(26, 'CAB_MAIRIE_HANN', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_HLM', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_HLM', 'VIEW'),
	(26, 'CAB_MAIRIE_HLM', 'RELOAD'),
	(26, 'CAB_MAIRIE_HLM', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_KEURMASSAR', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_KEURMASSAR', 'VIEW'),
	(26, 'CAB_MAIRIE_KEURMASSAR', 'RELOAD'),
	(26, 'CAB_MAIRIE_KEURMASSAR', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_MALIKA', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_MALIKA', 'VIEW'),
	(26, 'CAB_MAIRIE_MALIKA', 'RELOAD'),
	(26, 'CAB_MAIRIE_MALIKA', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_MBAO', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_MBAO', 'VIEW'),
	(26, 'CAB_MAIRIE_MBAO', 'RELOAD'),
	(26, 'CAB_MAIRIE_MBAO', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW'),
	(26, 'CAB_MAIRIE_MEDGOUNASS', 'RELOAD'),
	(26, 'CAB_MAIRIE_MEDGOUNASS', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_MEDINA', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_MEDINA', 'VIEW'),
	(26, 'CAB_MAIRIE_MEDINA', 'RELOAD'),
	(26, 'CAB_MAIRIE_MEDINA', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW'),
	(26, 'CAB_MAIRIE_LIMAMOULAYE', 'RELOAD'),
	(26, 'CAB_MAIRIE_LIMAMOULAYE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_OUAKAM', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_OUAKAM', 'VIEW'),
	(26, 'CAB_MAIRIE_OUAKAM', 'RELOAD'),
	(26, 'CAB_MAIRIE_OUAKAM', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_PA', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_PA', 'VIEW'),
	(26, 'CAB_MAIRIE_PA', 'RELOAD'),
	(26, 'CAB_MAIRIE_PA', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_PATTEDOIE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_PATTEDOIE', 'VIEW'),
	(26, 'CAB_MAIRIE_PATTEDOIE', 'RELOAD'),
	(26, 'CAB_MAIRIE_PATTEDOIE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_PIKEST', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_PIKEST', 'VIEW'),
	(26, 'CAB_MAIRIE_PIKEST', 'RELOAD'),
	(26, 'CAB_MAIRIE_PIKEST', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_PIKOUEST', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_PIKOUEST', 'VIEW'),
	(26, 'CAB_MAIRIE_PIKOUEST', 'RELOAD'),
	(26, 'CAB_MAIRIE_PIKOUEST', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_PIKNORD', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_PIKNORD', 'VIEW'),
	(26, 'CAB_MAIRIE_PIKNORD', 'RELOAD'),
	(26, 'CAB_MAIRIE_PIKNORD', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_PLATEAU', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_PLATEAU', 'VIEW'),
	(26, 'CAB_MAIRIE_PLATEAU', 'RELOAD'),
	(26, 'CAB_MAIRIE_PLATEAU', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_NOTAIRE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_NOTAIRE', 'VIEW'),
	(26, 'CAB_MAIRIE_NOTAIRE', 'RELOAD'),
	(26, 'CAB_MAIRIE_NOTAIRE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_SICAP', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_SICAP', 'VIEW'),
	(26, 'CAB_MAIRIE_SICAP', 'RELOAD'),
	(26, 'CAB_MAIRIE_SICAP', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_THIAMER', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_THIAMER', 'VIEW'),
	(26, 'CAB_MAIRIE_THIAMER', 'RELOAD'),
	(26, 'CAB_MAIRIE_THIAMER', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_THIAGARE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_THIAGARE', 'VIEW'),
	(26, 'CAB_MAIRIE_THIAGARE', 'RELOAD'),
	(26, 'CAB_MAIRIE_THIAGARE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_WAKHINANE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_WAKHINANE', 'VIEW'),
	(26, 'CAB_MAIRIE_WAKHINANE', 'RELOAD'),
	(26, 'CAB_MAIRIE_WAKHINANE', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_YEUMNORD', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_YEUMNORD', 'VIEW'),
	(26, 'CAB_MAIRIE_YEUMNORD', 'RELOAD'),
	(26, 'CAB_MAIRIE_YEUMNORD', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_YEUMSUD', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_YEUMSUD', 'VIEW'),
	(26, 'CAB_MAIRIE_YEUMSUD', 'RELOAD'),
	(26, 'CAB_MAIRIE_YEUMSUD', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_YOFF', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_YOFF', 'VIEW'),
	(26, 'CAB_MAIRIE_YOFF', 'RELOAD'),
	(26, 'CAB_MAIRIE_YOFF', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_MERMOZ', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_MERMOZ', 'VIEW'),
	(26, 'CAB_MAIRIE_MERMOZ', 'RELOAD'),
	(26, 'CAB_MAIRIE_MERMOZ', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_BARGNY', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_BARGNY', 'VIEW'),
	(26, 'CAB_MAIRIE_BARGNY', 'RELOAD'),
	(26, 'CAB_MAIRIE_BARGNY', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_BAMBYLOR', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_BAMBYLOR', 'VIEW'),
	(26, 'CAB_MAIRIE_BAMBYLOR', 'RELOAD'),
	(26, 'CAB_MAIRIE_BAMBYLOR', 'DOWNLOAD'),

	(26, 'CAB_MAIRIE_YENE', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_YENE', 'VIEW'),
	(26, 'CAB_MAIRIE_YENE', 'RELOAD'),
	(26, 'CAB_MAIRIE_YENE', 'DOWNLOAD'),


	(26, 'CAB_MAIRIE_SENDOU', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_SENDOU', 'VIEW'),
	(26, 'CAB_MAIRIE_SENDOU', 'RELOAD'),
	(26, 'CAB_MAIRIE_SENDOU', 'DOWNLOAD'),


	(26, 'CAB_MAIRIE_SANGALKAM', 'VIEW_HISTORY'),
	(26, 'CAB_MAIRIE_SANGALKAM', 'VIEW'),
	(26, 'CAB_MAIRIE_SANGALKAM', 'RELOAD'),
	(26, 'CAB_MAIRIE_SANGALKAM', 'DOWNLOAD'),


	(27, 'CAB_MAIRIE_NGOR', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_NGOR', 'VIEW'),
	(27, 'CAB_MAIRIE_NGOR', 'RELOAD'),
	(27, 'CAB_MAIRIE_NGOR', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_BISCUITERIE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_BISCUITERIE', 'VIEW'),
	(27, 'CAB_MAIRIE_BISCUITERIE', 'RELOAD'),
	(27, 'CAB_MAIRIE_BISCUITERIE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_CAMBERENE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_CAMBERENE', 'VIEW'),
	(27, 'CAB_MAIRIE_CAMBERENE', 'RELOAD'),
	(27, 'CAB_MAIRIE_CAMBERENE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_COLOBANE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_COLOBANE', 'VIEW'),
	(27, 'CAB_MAIRIE_COLOBANE', 'RELOAD'),
	(27, 'CAB_MAIRIE_COLOBANE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_DALIFORT', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_DALIFORT', 'VIEW'),
	(27, 'CAB_MAIRIE_DALIFORT', 'RELOAD'),
	(27, 'CAB_MAIRIE_DALIFORT', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_DIACKSAO', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_DIACKSAO', 'VIEW'),
	(27, 'CAB_MAIRIE_DIACKSAO', 'RELOAD'),
	(27, 'CAB_MAIRIE_DIACKSAO', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_DIAMEGUENE', 'VIEW'),
	(27, 'CAB_MAIRIE_DIAMEGUENE', 'RELOAD'),
	(27, 'CAB_MAIRIE_DIAMEGUENE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_DIEUPPEUL', 'VIEW'),
	(27, 'CAB_MAIRIE_DIEUPPEUL', 'RELOAD'),
	(27, 'CAB_MAIRIE_DIEUPPEUL', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_THIAROYEKAO', 'VIEW'),
	(27, 'CAB_MAIRIE_THIAROYEKAO', 'RELOAD'),
	(27, 'CAB_MAIRIE_THIAROYEKAO', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_FANN', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_FANN', 'VIEW'),
	(27, 'CAB_MAIRIE_FANN', 'RELOAD'),
	(27, 'CAB_MAIRIE_FANN', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_GOLFSUD', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_GOLFSUD', 'VIEW'),
	(27, 'CAB_MAIRIE_GOLFSUD', 'RELOAD'),
	(27, 'CAB_MAIRIE_GOLFSUD', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_GOREE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_GOREE', 'VIEW'),
	(27, 'CAB_MAIRIE_GOREE', 'RELOAD'),
	(27, 'CAB_MAIRIE_GOREE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_GRANDAKAR', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_GRANDAKAR', 'VIEW'),
	(27, 'CAB_MAIRIE_GRANDAKAR', 'RELOAD'),
	(27, 'CAB_MAIRIE_GRANDAKAR', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_GRANDYOFF', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_GRANDYOFF', 'VIEW'),
	(27, 'CAB_MAIRIE_GRANDYOFF', 'RELOAD'),
	(27, 'CAB_MAIRIE_GRANDYOFF', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_GUINORD', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_GUINORD', 'VIEW'),
	(27, 'CAB_MAIRIE_GUINORD', 'RELOAD'),
	(27, 'CAB_MAIRIE_GUINORD', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_GUISUD', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_GUISUD', 'VIEW'),
	(27, 'CAB_MAIRIE_GUISUD', 'RELOAD'),
	(27, 'CAB_MAIRIE_GUISUD', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_HANN', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_HANN', 'VIEW'),
	(27, 'CAB_MAIRIE_HANN', 'RELOAD'),
	(27, 'CAB_MAIRIE_HANN', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_HLM', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_HLM', 'VIEW'),
	(27, 'CAB_MAIRIE_HLM', 'RELOAD'),
	(27, 'CAB_MAIRIE_HLM', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_KEURMASSAR', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_KEURMASSAR', 'VIEW'),
	(27, 'CAB_MAIRIE_KEURMASSAR', 'RELOAD'),
	(27, 'CAB_MAIRIE_KEURMASSAR', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_MALIKA', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_MALIKA', 'VIEW'),
	(27, 'CAB_MAIRIE_MALIKA', 'RELOAD'),
	(27, 'CAB_MAIRIE_MALIKA', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_MBAO', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_MBAO', 'VIEW'),
	(27, 'CAB_MAIRIE_MBAO', 'RELOAD'),
	(27, 'CAB_MAIRIE_MBAO', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_MEDGOUNASS', 'VIEW'),
	(27, 'CAB_MAIRIE_MEDGOUNASS', 'RELOAD'),
	(27, 'CAB_MAIRIE_MEDGOUNASS', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_MEDINA', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_MEDINA', 'VIEW'),
	(27, 'CAB_MAIRIE_MEDINA', 'RELOAD'),
	(27, 'CAB_MAIRIE_MEDINA', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_LIMAMOULAYE', 'VIEW'),
	(27, 'CAB_MAIRIE_LIMAMOULAYE', 'RELOAD'),
	(27, 'CAB_MAIRIE_LIMAMOULAYE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_OUAKAM', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_OUAKAM', 'VIEW'),
	(27, 'CAB_MAIRIE_OUAKAM', 'RELOAD'),
	(27, 'CAB_MAIRIE_OUAKAM', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_PA', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_PA', 'VIEW'),
	(27, 'CAB_MAIRIE_PA', 'RELOAD'),
	(27, 'CAB_MAIRIE_PA', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_PATTEDOIE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_PATTEDOIE', 'VIEW'),
	(27, 'CAB_MAIRIE_PATTEDOIE', 'RELOAD'),
	(27, 'CAB_MAIRIE_PATTEDOIE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_PIKEST', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_PIKEST', 'VIEW'),
	(27, 'CAB_MAIRIE_PIKEST', 'RELOAD'),
	(27, 'CAB_MAIRIE_PIKEST', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_PIKOUEST', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_PIKOUEST', 'VIEW'),
	(27, 'CAB_MAIRIE_PIKOUEST', 'RELOAD'),
	(27, 'CAB_MAIRIE_PIKOUEST', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_PIKNORD', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_PIKNORD', 'VIEW'),
	(27, 'CAB_MAIRIE_PIKNORD', 'RELOAD'),
	(27, 'CAB_MAIRIE_PIKNORD', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_PLATEAU', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_PLATEAU', 'VIEW'),
	(27, 'CAB_MAIRIE_PLATEAU', 'RELOAD'),
	(27, 'CAB_MAIRIE_PLATEAU', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_NOTAIRE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_NOTAIRE', 'VIEW'),
	(27, 'CAB_MAIRIE_NOTAIRE', 'RELOAD'),
	(27, 'CAB_MAIRIE_NOTAIRE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_SICAP', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_SICAP', 'VIEW'),
	(27, 'CAB_MAIRIE_SICAP', 'RELOAD'),
	(27, 'CAB_MAIRIE_SICAP', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_THIAMER', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_THIAMER', 'VIEW'),
	(27, 'CAB_MAIRIE_THIAMER', 'RELOAD'),
	(27, 'CAB_MAIRIE_THIAMER', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_THIAGARE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_THIAGARE', 'VIEW'),
	(27, 'CAB_MAIRIE_THIAGARE', 'RELOAD'),
	(27, 'CAB_MAIRIE_THIAGARE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_WAKHINANE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_WAKHINANE', 'VIEW'),
	(27, 'CAB_MAIRIE_WAKHINANE', 'RELOAD'),
	(27, 'CAB_MAIRIE_WAKHINANE', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_YEUMNORD', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_YEUMNORD', 'VIEW'),
	(27, 'CAB_MAIRIE_YEUMNORD', 'RELOAD'),
	(27, 'CAB_MAIRIE_YEUMNORD', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_YEUMSUD', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_YEUMSUD', 'VIEW'),
	(27, 'CAB_MAIRIE_YEUMSUD', 'RELOAD'),
	(27, 'CAB_MAIRIE_YEUMSUD', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_YOFF', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_YOFF', 'VIEW'),
	(27, 'CAB_MAIRIE_YOFF', 'RELOAD'),
	(27, 'CAB_MAIRIE_YOFF', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_MERMOZ', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_MERMOZ', 'VIEW'),
	(27, 'CAB_MAIRIE_MERMOZ', 'RELOAD'),
	(27, 'CAB_MAIRIE_MERMOZ', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_BARGNY', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_BARGNY', 'VIEW'),
	(27, 'CAB_MAIRIE_BARGNY', 'RELOAD'),
	(27, 'CAB_MAIRIE_BARGNY', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_BAMBYLOR', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_BAMBYLOR', 'VIEW'),
	(27, 'CAB_MAIRIE_BAMBYLOR', 'RELOAD'),
	(27, 'CAB_MAIRIE_BAMBYLOR', 'DOWNLOAD'),

	(27, 'CAB_MAIRIE_YENE', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_YENE', 'VIEW'),
	(27, 'CAB_MAIRIE_YENE', 'RELOAD'),
	(27, 'CAB_MAIRIE_YENE', 'DOWNLOAD'),


	(27, 'CAB_MAIRIE_SENDOU', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_SENDOU', 'VIEW'),
	(27, 'CAB_MAIRIE_SENDOU', 'RELOAD'),
	(27, 'CAB_MAIRIE_SENDOU', 'DOWNLOAD'),


	(27, 'CAB_MAIRIE_SANGALKAM', 'VIEW_HISTORY'),
	(27, 'CAB_MAIRIE_SANGALKAM', 'VIEW'),
	(27, 'CAB_MAIRIE_SANGALKAM', 'RELOAD'),
	(27, 'CAB_MAIRIE_SANGALKAM', 'DOWNLOAD');


-- Export de la structure de la table teledac_acte3. pieces

-- Export de données de la table teledac_acte3.pieces : ~23 rows (environ)
/*!40000 ALTER TABLE `pieces` DISABLE KEYS */;
INSERT INTO `pieces` (`PIE_ID`, `PIE_DESCRIPTION`, `PIE_FORMAT`, `PIE_LIBELLE`, `PIE_REQUIRED`, `PIE_TYPE`, `PIE_PROC_ID`, `PIE_TRANS_ID`, `PIE_Quittance`, `PIE_TAG`) VALUES
	(25, 'Avis techniques', 'PDF', 'Avis techniques', 0, 'AVIS_TECHNIQUE', 1, NULL, NULL, 'AVT'),
	(26, '', 'PDF', 'Arrêté d\'autorisation de construire_VDUA', 0, 'ACTE', 1, 112, 0, NULL),
	(27, '', 'PDF', 'Arrêté d\'autorisation de construire_RDUA', 0, 'ACTE', 1, 108, 0, NULL);
/*!40000 ALTER TABLE `pieces` ENABLE KEYS */;


-- Export de données de la table teledac_acte3.procedures_organisations : ~101 rows (environ)
/*!40000 ALTER TABLE `procedures_organisations` DISABLE KEYS */;
INSERT INTO `procedures_organisations` (`PO_ID`, `PO_RESPONSABLE`, `PO_ORG_CODE`, `PO_PROC_ID`) VALUES
	
	(59, 1, 'SOUSPREF_ALMADIES', 1),
	(60, 1, 'MAIRIE_NGOR', 1),
	(61, 1, 'MAIRIE_OUAKAM', 1),
	(62, 1, 'SOUSPREF_DAKARPLATEAU', 1),
	(63, 1, 'MAIRIE_MEDINA', 1),
	(64, 1, 'MAIRIE_PLATEAU', 1),
	(65, 1, 'SOUSPREF_GUEDIAWAYE', 1),
	(66, 1, 'MAIRIE_GOLFSUD', 1),
	(67, 1, 'MAIRIE_MEDGOUNASS', 1),
	(68, 1, 'SOUSPREF_NIAYES', 1),
	(69, 1, 'MAIRIE_KEURMASSAR', 1),
	(70, 1, 'MAIRIE_MALIKA', 1),
	(71, 1, 'SOUSPREF_THIAROYE', 1),
	(72, 1, 'MAIRIE_DIACKSAO', 1),
	(73, 1, 'MAIRIE_MBAO', 1),
	(76, 1, 'MAIRIE_YENE', 1),
	(80, 1, 'MAIRIE_YOFF', 1),
	(81, 1, 'MAIRIE_COLOBANE', 1),
	(82, 1, 'MAIRIE_MERMOZ', 1),
	(83, 1, 'MAIRIE_GOREE', 1),
	(84, 1, 'MAIRIE_FANN', 1),
	(85, 1, 'MAIRIE_NOTAIRE', 1),
	(86, 1, 'MAIRIE_LIMAMOULAYE', 1),
	(87, 1, 'MAIRIE_WAKHINANE', 1),
	(88, 1, 'MAIRIE_YEUMNORD', 1),
	(89, 1, 'MAIRIE_YEUMBEULSUD', 1),
	(90, 1, 'MAIRIE_DIAMEGUENE', 1),
	(91, 1, 'MAIRIE_THIAMER', 1),
	(92, 1, 'MAIRIE_THIAGARE', 1),
	(93, 1, 'SOUSPREF_PA', 1),
	(94, 1, 'MAIRIE_CAMBERENE', 1),
	(95, 1, 'MAIRIE_GRANDYOFF', 1),
	(96, 1, 'MAIRIE_PA', 1),
	(97, 1, 'MAIRIE_PATTEDOIE', 1),
	(98, 1, 'SOUSPREF_GRDAK', 1),
	(99, 1, 'SOUSPREF_DAGOUDANE', 1),
	(100, 1, 'MAIRIE_DALIFORT', 1),
	(101, 1, 'MAIRIE_BISCUITERIE', 1),
	(102, 1, 'MAIRIE_DIEUPPEUL', 1),
	(103, 1, 'MAIRIE_GRANDAKAR', 1),
	(104, 1, 'MAIRIE_SICAP', 1),
	(105, 1, 'MAIRIE_HANN', 1),
	(106, 1, 'MAIRIE_HLM', 1),
	(107, 1, 'MAIRIE_THIAROYEKAO', 1),
	(108, 1, 'MAIRIE_GUINORD', 1),
	(109, 1, 'MAIRIE_GUISUD', 1),
	(110, 1, 'MAIRIE_PIKEST', 1),
	(111, 1, 'CABMJN', 1),
	(112, 1, 'CABMCS', 1),
	(113, 1, 'CABMTP', 1),
	(114, 1, 'CABMB', 1),
	(116, 1, 'CABMY', 1),
	(118, 1, 'PREFET_RUFISQUE', 1),
	(121, 1, 'SOUSPREFECTURE_RUFISQUEEST', 1),
	(122, 1, 'CABMSE', 1),
	(123, 1, 'CABMD', 1),
	(124, 1, 'CABMSEB', 1),
	(125, 1, 'MAIRIE_RUFISQUE_OUEST', 1),
	(126, 1, 'MAIRIE_RUFISQUE_NORD', 1),
	(128, 1, 'MAIRIE_RUFISQUE_EST', 1),
	(129, 1, 'MAIRIE_BARGNY', 1),
	(130, 0, 'DUA', 1),
	(131, 1, 'SOUSPREF_BAMBYLOR', 1),
        (132, 1, 'MAIRIE_PIKOUEST', 1),
        (133, 1, 'MAIRIE_PIKNORD', 1),
        (134, 1, 'MAIRIE_RUFISk_EST', 1);
/*!40000 ALTER TABLE `procedures_organisations` ENABLE KEYS */;


/*!40000 ALTER TABLE `transitions` DISABLE KEYS */;
INSERT INTO `transitions` (`TRS_ID`, `TRS_DUREE`, `TRS_NAME`, `TRS_STATUT`, `TRS_ETAT_FINAL_CODE`, `TRS_ETAT_INITIAL_CODE`, `IS_VIEW`, `TRS_STARTER`) VALUES

	/*(89, NULL, 'Réintroduire', 1, 'REINTRODUCTION', 'COMPLETED', 0, 0),non existant en demo mais existe en production, a valider*/
	(96, NULL, 'Renseigner Taxe', 1, 'EN_ATTENTE_PAIEMENT', 'SOUMIS_PAIEMENT', 0, 0),
	(97, NULL, 'Visualiser', 1, NULL, 'SOUMIS_PAIEMENT', 1, 0),
	(98, NULL, 'Payer', 1, 'PENDING', 'EN_ATTENTE_PAIEMENT', 0, 0),
	(99, NULL, 'Visualiser', 1, NULL, 'EN_ATTENTE_PAIEMENT', 1, 0),
	(100, NULL, 'Accepter avec transfert', 1, 'RECEVABLE', 'PENDING', 0, 0),
	(101, NULL, 'Approuver avec transfert', 1, 'SIGNED_MAIRIE', 'COMPLETED', 0, 0),
	(102, NULL, 'Demander avis DUA', 1, 'EN_ATTENTE_AVIS_DUA', 'COMPLETED', 0, 0),
	(103, NULL, 'Valider', 1, 'VALIDE_DUA', 'EN_ATTENTE_AVIS_DUA', 0, 0),
	(104, NULL, 'Rejeter', 1, 'REJET_DUA', 'EN_ATTENTE_AVIS_DUA', 0, 0),
	
	(105, NULL, 'Visualiser', 1, NULL, 'EN_ATTENTE_AVIS_DUA', 1, 0),
	(106, NULL, 'Visualiser', 1, NULL, 'REJET_DUA', 1, 0),
	(107, NULL, 'Visualiser', 1, NULL, 'VALIDE_DUA', 1, 0),
	(108, NULL, 'Accepter', 1, 'SIGNED_MAIRIE', 'REJET_DUA', 0, 0),
	(109, NULL, 'Rejeter définitivement', 1, 'REJETED', 'REJET_DUA', 0, 0),
	(110, NULL, 'Suspendre', 1, 'SUSPENDED', 'REJET_DUA', 0, 0),
	(111, NULL, 'Réintroduire', 1, 'REINTRODUCTION', 'REJET_DUA', 0, 0),
	(112, NULL, 'Accepter', 1, 'SIGNED_MAIRIE', 'VALIDE_DUA', 0, 0),
	
	(113, NULL, 'Rejeter définitivement', 1, 'REJETED', 'VALIDE_DUA', 0, 0),
	(114, NULL, 'Suspendre', 1, 'SUSPENDED', 'VALIDE_DUA', 0, 0),
	(115, NULL, 'Réintroduire', 1, 'REINTRODUCTION', 'VALIDE_DUA', 0, 0),
	(116, NULL, 'Imputer', 1, 'EN_ATTENTE_AVIS_DUA', 'EN_ATTENTE_AVIS_DUA', 0, NULL),
	(117, NULL, 'Approuver avec transfert', 1, 'SIGNED_MAIRIE', 'REJET_DUA', 0, 0),
	(118, NULL, 'Approuver avec transfert', 1, 'SIGNED_MAIRIE', 'VALIDE_DUA', 0, 0);
/*!40000 ALTER TABLE `transitions` ENABLE KEYS */;

INSERT INTO `sites` (`SIT_CODE`, `SIT_DESCRIPTION`, `SIT_LIBELLE`, `SIT_PAR_CODE`, `SIT_TS_CODE`) VALUES 
('RUFISQUE_PREFECTURE', 'Prefecture de rufisque', 'Préfécture de rufisque', 'RUFISQUE', 'ARRONDISSEMENT');

UPDATE `sites` SET `SIT_PAR_CODE`='RUFISQUE_PREFECTURE' WHERE  `SIT_CODE`='COM. BARGNY_CA';

UPDATE `sites` SET `SIT_PAR_CODE`='RUFISQUE_PREFECTURE' WHERE  `SIT_CODE`='COM. SEBIKOTANE_CA';
UPDATE `sites` SET `SIT_PAR_CODE`='RUFISQUE_PREFECTURE' WHERE  `SIT_CODE`='COM. SENDOU_CA';
UPDATE `sites` SET `SIT_PAR_CODE`='BAMBYLOR_ARRONDISSEMENT' WHERE  `SIT_CODE`='COM. DIAMNIADIO_CA';
UPDATE `sites` SET `SIT_PAR_CODE`='BAMBYLOR_ARRONDISSEMENT' WHERE  `SIT_CODE`='COM. JAXAAY PARCELLE NIAKOUL RAP_CA';


/* code integrés le 01/08/2016 */

UPDATE `organisations` SET `ORG_SIT_CODE`='GUEDIAWAYE' WHERE  `ORG_CODE`='DOM_PG';
UPDATE `organisations` SET `ORG_SIT_CODE`='GUEDIAWAYE' WHERE  `ORG_CODE`='CAD_PG';

INSERT INTO `delais` (`DEL_DESC`, `DEL_DUREE`, `DEL_STATUT`, `DEL_ETA_CODE`) VALUES ('Délai d\'alerte aprés 48h sans traitement du dossier', '48', 1, 'VALIDE_DUA');
INSERT INTO `delais` (`DEL_DESC`, `DEL_DUREE`, `DEL_STATUT`, `DEL_ETA_CODE`) VALUES ('Délai d\'alerte aprés 48h sans traitement du dossier', '48', 1, 'REJET_DUA');
INSERT INTO `delais` (`DEL_DESC`, `DEL_DUREE`, `DEL_STATUT`, `DEL_ETA_CODE`) VALUES ('Délai d\'alerte aprés 48h sans traitement du dossier', '48', 1, 'EN_ATTENTE_AVIS_DUA');

/* Integration scripts Soxna 03/08/2016 */

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =14 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =19 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSRUD" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDUP" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDU_GW" and  `FPA_PA_CODE`= "VIEW_HISTORY";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "RELOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "DOWNLOAD";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CSDU_RF" and  `FPA_PA_CODE`= "VIEW_HISTORY";
	
 /* delete fonctions permissions qui existent et qui doivent être supprimées pour certaines fonctions */
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_GW" and `FP_PERM_ID`=47;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_GW" and `FP_PERM_ID`=46;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_GW" and `FP_PERM_ID`=57;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_GW" and `FP_PERM_ID`=58;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_GW" and `FP_PERM_ID`=50;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_GW" and `FP_PERM_ID`=51;

DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_RF" and `FP_PERM_ID`=47;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_RF" and `FP_PERM_ID`=51;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_RF" and `FP_PERM_ID`=57;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_RF" and `FP_PERM_ID`=58;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_RF" and `FP_PERM_ID`=46;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDU_RF" and `FP_PERM_ID`=50;

DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDUP" and `FP_PERM_ID`=47;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDUP" and `FP_PERM_ID`=51;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDUP" and `FP_PERM_ID`=46;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDUP" and `FP_PERM_ID`=50;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDUP" and `FP_PERM_ID`=57;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSDUP" and `FP_PERM_ID`=58;

DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSRUD" and `FP_PERM_ID`=46;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSRUD" and `FP_PERM_ID`=50;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSRUD" and `FP_PERM_ID`=47;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSRUD" and `FP_PERM_ID`=51;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSRUD" and `FP_PERM_ID`=57;
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="CSRUD" and `FP_PERM_ID`=58;

insert into  permissions (`PERM_ID`, PERM_NAME,PERM_ETA_CODE,PERM_STATUT,PERM_TRS_ID) 
	values(119, "Permission de Imputer","APPROBATION_PREFET",1,119 ) ;

insert into fonctions_permissions (`FP_FCT_CODE`, `FP_PERM_ID`) 
	values 
	("PREFET_RUFISQUE",119),
	("SOUS_PREF_BAMBYLOR",119),
	("SOUSPREF_RUFISQUEEST",119),

	("SOUS_PREF_PA",119),
	("SOUS_PREF_DAKPLATEAU",119),
	("SOUS_PREF_GRDAK",119),
	("SOUS_PREF_ALMADIES",119),

	("SOUS_PREF_DAGOUDANE",119),
	("SOUS_PREF_THIAROYE",119),
	("SOUS_PREF_NIAYES",119),

	("SOUS_PREF_GUEDIAWAYE",119);

/*Organisation Mairie de rufisque a enlever, no longer exist in acte3*/
UPDATE `organisations` SET `ORG_STATUT`=0 WHERE  `ORG_CODE`='CABMR';


/*Mettre l'organisation off*/
UPDATE `organisations` SET `ORG_STATUT`=0 WHERE  `ORG_CODE`='SPG';


/* mise à niveau script 05/08/2016 */

insert into transitions (`TRS_ID`,TRS_NAME,TRS_STATUT,TRS_ETAT_FINAL_CODE,TRS_ETAT_INITIAL_CODE,IS_VIEW,TRS_STARTER) values
(119,"permission de Imputer",1,"APPROBATION_PREFET","APPROBATION_PREFET",0,0);



UPDATE `etats` SET `ETA_LIBELLE`="Arrêté d\'autorisation en approbation du représentant de l\'état" 
WHERE  `ETA_CODE`="APPROBATION_PREFET";

UPDATE `etats` SET `ETA_LIBELLE`="Retourné par le représentant de l\'état" WHERE  `ETA_CODE`='Retour_Prefet';

update transitions set TRS_NAME ="Imputer" where TRS_ID=119;

/* 05-08-2016 Moussa*/
/* Enlever la transversalité de sapeur pompier rufisque*/
UPDATE `organisations` SET `ORG_TRANSVERSALE`=0 WHERE  `ORG_CODE`='SPR';
/*Mettre à off l'organisation TEST, pas besoin de RA*/
UPDATE `organisations` SET `ORG_STATUT`=0 WHERE  `ORG_CODE`='ORG_TEST';



/*Enlever les permissions à la fonction SDE*/
DELETE FROM `fonctions_permissions` WHERE  `FP_FCT_CODE`="SDE";
/*Mettre les users>Agent sap pikine/guediawaye off*/
UPDATE `utilisateurs` SET `USR_STATUT`=0 WHERE `USR_FCT_CODE`='ASP_GW';


/* 05-08-2016 Moussa, PERMISSIONS DE DUA*/
INSERT INTO `fonctions_permissions` (`FP_FCT_CODE`, `FP_PERM_ID`) VALUES
	
	('DUA', 106),
	('DUA', 107),
	('DUA', 116),
	('DUA', 105),
	('DUA', 104),
	('DUA', 103);


	DELETE FROM `fonctions_permissions` WHERE `FP_PERM_ID`=100;
	DELETE FROM `fonctions_permissions` WHERE `FP_PERM_ID`=101;
	DELETE FROM `fonctions_permissions` WHERE `FP_PERM_ID`=117;
	DELETE FROM `fonctions_permissions` WHERE `FP_PERM_ID`=118;
/*by sokhna le 11/08/2016 :Permissions actes manquantes*/
INSERT INTO `fonctions_permissions_actes` (`FPA_PIE_ID`, `FPA_FCT_CODE`, `FPA_PA_CODE`) VALUES
(18,"CAB_MAIRIE_TIVAOUNE_PEULH","RELOAD"),
(18,"CAB_MAIRIE_TIVAOUNE_PEULH","VIEW_HISTORY"),
(18,"CAB_MAIRIE_TIVAOUNE_PEULH","DOWNLOAD"),

(27,"CAB_MAIRIE_TIVAOUNE_PEULH","RELOAD"),
(27,"CAB_MAIRIE_TIVAOUNE_PEULH","VIEW_HISTORY"),
(27,"CAB_MAIRIE_TIVAOUNE_PEULH","DOWNLOAD"),

(10,"CAB_MAIRIE_TIVAOUNE_PEULH","RELOAD"),
(10,"CAB_MAIRIE_TIVAOUNE_PEULH","VIEW_HISTORY"),
(10,"CAB_MAIRIE_TIVAOUNE_PEULH","DOWNLOAD"),

(26,"CAB_MAIRIE_TIVAOUNE_PEULH","RELOAD"),
(26,"CAB_MAIRIE_TIVAOUNE_PEULH","VIEW_HISTORY"),
(26,"CAB_MAIRIE_TIVAOUNE_PEULH","DOWNLOAD"),

(18,"CAB_MAIRIE_JAXAAY","RELOAD"),
(18,"CAB_MAIRIE_JAXAAY","VIEW_HISTORY"),
(18,"CAB_MAIRIE_JAXAAY","DOWNLOAD"),

(27,"CAB_MAIRIE_JAXAAY","RELOAD"),
(27,"CAB_MAIRIE_JAXAAY","VIEW_HISTORY"),
(27,"CAB_MAIRIE_JAXAAY","DOWNLOAD"),

(26,"CAB_MAIRIE_JAXAAY","RELOAD"),
(26,"CAB_MAIRIE_JAXAAY","VIEW_HISTORY"),
(26,"CAB_MAIRIE_JAXAAY","DOWNLOAD");

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CAB_MAIRIE_BAMBYLOR" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CAB_MAIRIE_BAMBYLOR" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =26 and  `FPA_FCT_CODE` ="CAB_MAIRIE_BAMBYLOR" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =27 and  `FPA_FCT_CODE` ="CAB_MAIRIE_BAMBYLOR" and  `FPA_PA_CODE`= "VIEW";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CAB_MAIRIE_DIAMNIADIO" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CAB_MAIRIE_DIAMNIADIO" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =26 and  `FPA_FCT_CODE` ="CAB_MAIRIE_DIAMNIADIO" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =27 and  `FPA_FCT_CODE` ="CAB_MAIRIE_DIAMNIADIO" and  `FPA_PA_CODE`= "VIEW";

delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =10 and  `FPA_FCT_CODE` ="CAB_MAIRIE_YENE" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =18 and  `FPA_FCT_CODE` ="CAB_MAIRIE_YENE" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =26 and  `FPA_FCT_CODE` ="CAB_MAIRIE_YENE" and  `FPA_PA_CODE`= "VIEW";
delete from  `fonctions_permissions_actes` where `FPA_PIE_ID` =27 and  `FPA_FCT_CODE` ="CAB_MAIRIE_YENE" and  `FPA_PA_CODE`= "VIEW";

	/*Sapeur Pikine Guédiawaye*/
/*On le rajoute comme procedure responsable*/
	INSERT INTO `procedures_organisations` (`PO_RESPONSABLE`, `PO_ORG_CODE`, `PO_PROC_ID`) VALUES (1, 'SP_PKG', 1);
/*On lui rajoute les permissions à seconde instruction qui sont manquantes dans le script en haut*/
/*On enleve la transversalité à la mauvaise ORGANISATION*/
UPDATE `organisations` SET `ORG_TRANSVERSALE`=0 WHERE  `ORG_CODE`='SPG';

/*By sokhna le 12/08/2016 --permissions restantes sur les Mairies et MAJ*/

INSERT INTO `fonctions_permissions_actes` (`FPA_PIE_ID`, `FPA_FCT_CODE`, `FPA_PA_CODE`) VALUES
(18,"CAB_MAIRIE_JAXAAY","RELOAD"),
(18,"CAB_MAIRIE_JAXAAY","VIEW_HISTORY"),
(18,"CAB_MAIRIE_JAXAAY","DOWNLOAD"),

(27,"CAB_MAIRIE_JAXAAY","RELOAD"),
(27,"CAB_MAIRIE_JAXAAY","VIEW_HISTORY"),
(27,"CAB_MAIRIE_JAXAAY","DOWNLOAD"),

(26,"CAB_MAIRIE_JAXAAY","RELOAD"),
(26,"CAB_MAIRIE_JAXAAY","VIEW_HISTORY"),
(26,"CAB_MAIRIE_JAXAAY","DOWNLOAD");


DELETE from fonctions_permissions  where FP_FCT_CODE ="CAB_MAIRIE_SANGALKAM";

insert into fonctions_permissions (`FP_FCT_CODE`,`FP_PERM_ID`) values 
("CAB_MAIRIE_SANGALKAM",84),
("CAB_MAIRIE_SANGALKAM",70),
("CAB_MAIRIE_SANGALKAM",58),
("CAB_MAIRIE_SANGALKAM",57),
("CAB_MAIRIE_SANGALKAM",83),
("CAB_MAIRIE_SANGALKAM",71),
("CAB_MAIRIE_SANGALKAM",50),
("CAB_MAIRIE_SANGALKAM",46);


/*Cas de diamniadio*/

DELETE from fonctions_permissions_actes  where FPA_FCT_CODE ="CAB_MAIRIE_DIAMNIADIO";

insert into fonctions_permissions_actes (`FPA_PIE_ID`,`FPA_FCT_CODE`,`FPA_PA_CODE`) values 
(10,"CAB_MAIRIE_DIAMNIADIO","RELOAD"),
(10,"CAB_MAIRIE_DIAMNIADIO","VIEW_HISTORY"),
(10,"CAB_MAIRIE_DIAMNIADIO","DOWNLOAD"),

(18,"CAB_MAIRIE_DIAMNIADIO","RELOAD"),
(18,"CAB_MAIRIE_DIAMNIADIO","VIEW_HISTORY"),
(18,"CAB_MAIRIE_DIAMNIADIO","DOWNLOAD"),

(27,"CAB_MAIRIE_DIAMNIADIO","RELOAD"),
(27,"CAB_MAIRIE_DIAMNIADIO","VIEW_HISTORY"),
(27,"CAB_MAIRIE_DIAMNIADIO","DOWNLOAD"),

(26,"CAB_MAIRIE_DIAMNIADIO","RELOAD"),
(26,"CAB_MAIRIE_DIAMNIADIO","VIEW_HISTORY"),
(26,"CAB_MAIRIE_DIAMNIADIO","DOWNLOAD");
/*cabinet du maire de rufisque ouest*/
insert into fonctions_permissions_actes (`FPA_PIE_ID`,`FPA_FCT_CODE`,`FPA_PA_CODE`) values 
(10,"CAB_MAIRIE_RUFIQUE_OUEST","RELOAD"),
(10,"CAB_MAIRIE_RUFIQUE_OUEST","VIEW_HISTORY"),
(10,"CAB_MAIRIE_RUFIQUE_OUEST","DOWNLOAD"),

(18,"CAB_MAIRIE_RUFIQUE_OUEST","RELOAD"),
(18,"CAB_MAIRIE_RUFIQUE_OUEST","VIEW_HISTORY"),
(18,"CAB_MAIRIE_RUFIQUE_OUEST","DOWNLOAD"),

(27,"CAB_MAIRIE_RUFIQUE_OUEST","RELOAD"),
(27,"CAB_MAIRIE_RUFIQUE_OUEST","VIEW_HISTORY"),
(27,"CAB_MAIRIE_RUFIQUE_OUEST","DOWNLOAD"),

(26,"CAB_MAIRIE_RUFIQUE_OUEST","RELOAD"),
(26,"CAB_MAIRIE_RUFIQUE_OUEST","VIEW_HISTORY"),
(26,"CAB_MAIRIE_RUFIQUE_OUEST","DOWNLOAD"),

(10,"CAB_MAIRIE_RUFISK_EST","RELOAD"),
(10,"CAB_MAIRIE_RUFISK_EST","VIEW_HISTORY"),
(10,"CAB_MAIRIE_RUFISK_EST","DOWNLOAD"),

(18,"CAB_MAIRIE_RUFISK_EST","RELOAD"),
(18,"CAB_MAIRIE_RUFISK_EST","VIEW_HISTORY"),
(18,"CAB_MAIRIE_RUFISK_EST","DOWNLOAD"),

(27,"CAB_MAIRIE_RUFISK_EST","RELOAD"),
(27,"CAB_MAIRIE_RUFISK_EST","VIEW_HISTORY"),
(27,"CAB_MAIRIE_RUFISK_EST","DOWNLOAD"),

(26,"CAB_MAIRIE_RUFISK_EST","RELOAD"),
(26,"CAB_MAIRIE_RUFISK_EST","VIEW_HISTORY"),
(26,"CAB_MAIRIE_RUFISK_EST","DOWNLOAD"),

(10,"CAB_MAIRIE_RUFISQUE_NORD","RELOAD"),
(10,"CAB_MAIRIE_RUFISQUE_NORD","VIEW_HISTORY"),
(10,"CAB_MAIRIE_RUFISQUE_NORD","DOWNLOAD"),

(18,"CAB_MAIRIE_RUFISQUE_NORD","RELOAD"),
(18,"CAB_MAIRIE_RUFISQUE_NORD","VIEW_HISTORY"),
(18,"CAB_MAIRIE_RUFISQUE_NORD","DOWNLOAD"),

(27,"CAB_MAIRIE_RUFISQUE_NORD","RELOAD"),
(27,"CAB_MAIRIE_RUFISQUE_NORD","VIEW_HISTORY"),
(27,"CAB_MAIRIE_RUFISQUE_NORD","DOWNLOAD"),

(26,"CAB_MAIRIE_RUFISQUE_NORD","RELOAD"),
(26,"CAB_MAIRIE_RUFISQUE_NORD","VIEW_HISTORY"),
(26,"CAB_MAIRIE_RUFISQUE_NORD","DOWNLOAD");

DELETE FROM fonctions_permissions  where FP_FCT_CODE ="CAB_MAIRIE_RUFIQUE_OUEST" and FP_PERM_ID=63;
DELETE FROM fonctions_permissions  where FP_FCT_CODE ="CAB_MAIRIE_RUFIQUE_OUEST" and FP_PERM_ID=51;
DELETE FROM fonctions_permissions  where FP_FCT_CODE ="CAB_MAIRIE_RUFIQUE_OUEST" and FP_PERM_ID=47;

insert into fonctions_permissions (`FP_FCT_CODE`,`FP_PERM_ID`) values 
("CAB_MAIRIE_RUFIQUE_OUEST",84),
("CAB_MAIRIE_RUFIQUE_OUEST",70),
("CAB_MAIRIE_RUFIQUE_OUEST",58),
("CAB_MAIRIE_RUFIQUE_OUEST",57);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;