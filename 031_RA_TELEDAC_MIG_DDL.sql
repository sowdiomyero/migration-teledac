/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- going back procedures
--
alter table `procedures` drop PROC_CODE_PROCEDURE;
alter table `procedures` drop telepaiementState;

--
-- going back organisations
--
alter table `organisations` drop telepaiementState;
alter table `organisations` drop ORG_STATUT; 

--
-- going back historiques
--
/*
ALTER TABLE  `historiques` DROP INDEX IDX_HIS_COMMUNE_CODE ;
alter table `historiques` drop foreign key FK_HIS_COMMUNE_CODE;
alter table `historiques` drop HIS_COMMUNE_CODE;
*/
--
-- going back dossiers
--
alter table `dossiers` drop foreign key FK_COMMUNEDEPOT_CODE;
alter table `dossiers` drop foreign key FK_MOY_TRANSF_ID;

alter table `dossiers` drop DOS_ACTEIII ;
alter table `dossiers` drop CODE_PAIEMENT ;
alter table `dossiers` drop telepaiementState ;
alter table `dossiers` drop DOS_TYPE ;
alter table `dossiers` drop DOS_COMMUNEDEPOT_CODE ;
alter table `dossiers` drop MOY_TRANSF_ID ;

--
-- drop table parametres
--
drop table `parametres`;

--
-- drop table typetaxes
--
drop table `typetaxes`;

--
-- drop table taxes
--
drop table `taxes`;

--
-- drop table sites_br_recouvrements
--
drop table `sites_br_recouvrements`;

--
-- drop table paiements
--
drop table `paiements`;

--
-- drop table numero_dossier
--
drop table `numero_dossier`;

--
-- drop table moyenstransferts
--
drop table `moyenstransferts`;

--
-- drop table demat_response_object
--
drop table `demat_response_object`;

--
-- drop table comptes
--
drop table `comptes`;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;