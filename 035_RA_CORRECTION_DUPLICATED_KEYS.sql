﻿/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;



DELETE FROM `fonctions_permissions` WHERE `FP_ID`=431; /* ok */

DELETE FROM `procedures_organisations` WHERE  `PO_ORG_CODE`="SOUSPREF_GUEDIAWAYE" ;
DELETE FROM `procedures_organisations` WHERE  `PO_ORG_CODE`="MAIRIE_MEDGOUNASS" ;

DELETE FROM `procedures_organisations` WHERE  `PO_ORG_CODE`="MAIRIE_SICAP" ;

DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=431 ;
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=294 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=295 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=296 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=297 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=299 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=300 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=301 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=302 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=303 ; /* DDUA*/

DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=345 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=346 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=347 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=348 ; /* DDUA*/

DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=348 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=425 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=426 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=427 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=428 ; /* DDUA*/

DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=428 ; /* DDUA*/

DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=429 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=430 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=432 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=443 ; /* DDUA*/

DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=450 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=457 ; /* DDUA*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=464 ; /* DDUA*/
/*Moussa: 04-08-2016*/
DELETE FROM `fonctions_permissions` WHERE  `FP_ID`=667 ; /* CSDU*/

DELETE FROM `pieces` WHERE PIE_ID=25;


DELETE FROM `transitions` WHERE  `TRS_ID`=97 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=99 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=105 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=106 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=107 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=109 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=111 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=113 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=115 ; /* insert effectué avec un id specifique*/
DELETE FROM `transitions` WHERE  `TRS_ID`=116 ; /* insert effectué avec un id specifique*/





/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;