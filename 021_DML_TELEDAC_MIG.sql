﻿
Update `dossiers` d
set d.`dos_type` ='NON_QUALIFIE';
/*
where d.`dos_id` IN (select  DISTINCT(h.his_dos_id)
    from `historiques` h
    where h.`HIS_ETAT_CODE` = 'DRAFT'
    OR h.`HIS_ETAT_CODE` = 'EN_ATTENTE_PAIEMENT'
    OR h.`HIS_ETAT_CODE` = 'SOUMIS_PAIEMENT');
*/


Update `dossiers` d
set d.`dos_type` ='ATTENTE_QUALIFICATION'
where d.`dos_id` IN (select  DISTINCT(h.`his_dos_id`)
    from `historiques` h
    where h.`HIS_ETAT_CODE` = 'PENDING'
    OR h.`HIS_ETAT_CODE` = 'RECEVABLE');


Update `dossiers` d
set d.`dos_type` ='SIMPLE'
where d.`dos_id` IN (select  DISTINCT(h.`his_dos_id`)
    from `historiques` h
    where h.`HIS_ETAT_CODE` = 'OPEN_DS'
    OR h.`HIS_ETAT_CODE` = 'INSTRUCTION_INTERNE'
    OR h.`HIS_ETAT_CODE` = 'Seconde_instruction');


Update dossiers d
set d.dos_type ='COMPLEXE'
where d.dos_id IN (select  DISTINCT(h.his_dos_id)
    from historiques h
    where h.`HIS_ETAT_CODE` = 'OPEN_DC');


INSERT INTO `numero_dossier` (`ND_ID`, `numero_current`, `numero_next`, `codeSite`) 
VALUES (NULL, 
	(SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'DAKAR_DEPARTEMENT' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE")),
 (SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'DAKAR_DEPARTEMENT' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE"))+1, 'DK');

INSERT INTO `numero_dossier` (`ND_ID`, `numero_current`, `numero_next`, `codeSite`) 
VALUES (NULL, 
	(SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'PIKINE' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE")),
 (SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'PIKINE' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE"))+1, 'PK');

INSERT INTO `numero_dossier` (`ND_ID`, `numero_current`, `numero_next`, `codeSite`) 
VALUES (NULL, 
	(SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'GUEDIAWAYE' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE")),
 (SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'GUEDIAWAYE' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE"))+1, 'GW');

INSERT INTO `numero_dossier` (`ND_ID`, `numero_current`, `numero_next`, `codeSite`) 
VALUES (NULL, 
	(SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'RUFISQUE' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE")),
 (SELECT COUNT( dos.DOS_ID ) FROM dossiers dos WHERE dos.DOS_SITEDEPOT_CODE = 'RUFISQUE' AND dos_date_depot >= "2016-01-01" AND dos.DOS_TYPE != ("NON_QUALIFIE"))+1, 'RK');

 

UPDATE  procedures p
SET  p.`PROC_CODE_PROCEDURE` =  'DAC' 
WHERE p.`PROC_ID` =1;
 
    

/* UPDATE `sites` SET `SIT_LIBELLE` = REPLACE(`SIT_LIBELLE` ,"Com. ","") WHERE `SIT_CODE` LIKE "Com. %"; */
/* UPDATE `sites` SET  `SIT_LIBELLE` = REPLACE(`SIT_LIBELLE` ,"Cr. ","") WHERE `SIT_CODE` LIKE "Cr. %"; */

UPDATE dossiers d SET d.DOS_COMMUNEDEPOT_CODE=(SELECT FORM_ARRONDISSEMENT_ID FROM formulaires f WHERE f.FORM_DOS_ID=d.DOS_ID GROUP BY d.DOS_ID); 	

/*RUFISQUE ARRONDISSEMENT*/
UPDATE `sites` SET `SIT_PAR_CODE`='BAMBYLOR_ARRONDISSEMENT' WHERE `SIT_PAR_CODE`='SANGALKAM_ARRONDISSEMENT';

--
-- Contenu de la table `sites_br_recouvrements`
--

INSERT INTO `sites_br_recouvrements` (`SITE_CODE`, `FONCTION_CODE`) VALUES
('CA. NGOR_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. OUAKAM_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. YOFF_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA.MERMOZ/ SACRE -COEUR_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. COLOBANE/FASS/GUEULE TAPEE_CA', 'ADOM_DakarPlateau'),
('CA. FANN/POINT E/ AMITIE_CA', 'ADOM_DakarPlateau'),
('CA. GOREE_CA', 'ADOM_DakarPlateau'),
('CA. MEDINA_CA', 'ADOM_DakarPlateau'),
('CA. PLATEAU_CA', 'ADOM_DakarPlateau'),
('CA. BISCUITERIE_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. DIEUPPEUL DERKLE_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. GRAND DAKAR_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. HANN/ BEL AIR _CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. HLM_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. SICAP LIBERTE_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. CAMBERENE_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. GRAND YOFF_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. PARCELLES ASSAINIES_CA', 'ADOM_Ngor_Almadie_GRDK'),
('CA. PATTE DOIE_CA', 'ADOM_Ngor_Almadie_GRDK'),
('COM. DIAMNIADIO_CA', 'AD_RF'),
('COM. JAXAAY PARCELLE NIAKOUL RAP_CA', 'AD_RF'),
('COM. SANGALKAM_CA', 'AD_RF'),
('CR. BAMBYLOR_CA', 'AD_RF'),
('CR. TIVAOUANE PEULH-NIAGHA_CA', 'AD_RF'),
('CR. YENE_CA', 'AD_RF'),
('CA. RUFISQUE CENTRE (NORD)_CA', 'AD_RF'),
('CA. RUFISQUE EST_CA', 'AD_RF'),
('CA. RUFISQUE OUEST_CA', 'AD_RF'),
('COM. BARGNY_CA', 'AD_RF'),
('COM. SEBIKOTANE_CA', 'AD_RF'),
('COM. SENDOU_CA', 'AD_RF'),
('COM. DIAMNIADIO_CA', 'AD_RF'),
('COM. JAXAAY PARCELLE NIAKOUL RAP_CA', 'AD_RF'),
('COM. SANGALKAM_CA', 'AD_RF'),
('CR. BAMBYLOR_CA', 'AD_RF'),
('CR. TIVAOUANE PEULH-NIAGHA_CA', 'AD_RF'),
('CR. YENE_CA', 'AD_RF'),
('CA. RUFISQUE CENTRE (NORD)_CA', 'AD_RF'),
('CA. RUFISQUE EST_CA', 'AD_RF'),
('CA. RUFISQUE OUEST_CA', 'AD_RF'),
('COM. BARGNY_CA', 'AD_RF'),
('COM. SEBIKOTANE_CA', 'AD_RF'),
('COM. SENDOU_CA', 'AD_RF'),
('COM. DIAMNIADIO_CA', 'AD_RF'),
('COM. JAXAAY PARCELLE NIAKOUL RAP_CA', 'AD_RF'),
('COM. SANGALKAM_CA', 'AD_RF'),
('CR. BAMBYLOR_CA', 'AD_RF'),
('CR. TIVAOUANE PEULH-NIAGHA_CA', 'AD_RF'),
('CR. YENE_CA', 'AD_RF'),
('CA. RUFISQUE CENTRE (NORD)_CA', 'AD_RF'),
('CA. RUFISQUE EST_CA', 'AD_RF'),
('CA. RUFISQUE OUEST_CA', 'AD_RF'),
('CA. MBAO_CA', 'AD_RF'),
('CA. THIAROYE /MER_CA', 'AD_RF'),
('CA. THIAROYE GARE_CA', 'AD_RF'),
('CA. GOLF SUD_CA', 'ADOM_PG'),
('CA. MEDINA GOUNASS_CA', 'ADOM_PG'),
('CA. NDIAREME LIMAMOULAYE_CA', 'ADOM_PG'),
('CA. SAM NOTAIRE_CA', 'ADOM_PG'),
('CA. WAKHINANE NIMZATT_CA', 'ADOM_PG'),
('CA. KEUR MASSAR_CA', 'ADOM_PG'),
('CA. MALIKA_CA', 'ADOM_PG'),
('CA. YEUMBEUL NORD_CA', 'ADOM_PG'),
('CA. YEUMBEUL SUD_CA', 'ADOM_PG'),
('CA. DALIFORD_CA', 'ADOM_PG'),
('CA. DJIDAH THIAROYE KAO_CA', 'ADOM_PG'),
('CA. GUINAW RAIL NORD_CA', 'ADOM_PG'),
('CA. GUINAW RAIL SUD_CA', 'ADOM_PG'),
('CA. PIKINE EST_CA', 'ADOM_PG'),
('CA. PIKINE OUEST_CA', 'ADOM_PG'),
('CA. PIKINE SUD/NORD_CA', 'ADOM_PG'),
('CA. DIACK SAO_CA', 'ADOM_PG'),
('CA. DIAMAGUENE/SICAP MBAO _CA', 'ADOM_PG'),
('CA. MBAO_CA', 'ADOM_PG'),
('CA. THIAROYE /MER_CA', 'ADOM_PG'),
('CA. THIAROYE GARE_CA', 'ADOM_PG');

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Contenu de la table `typetaxes`
--

INSERT INTO `typetaxes` (`TYPT_ID`, `TYPT_CODE`, `TYPT_DESCRIPTION`, `TYPT_LIBELLE`, `telepaiementState`, `TYPT_UNITE_MONETAIRE`, `TYPT_ORG_CODE`, `TYPT_PROC_ID`) VALUES
(1, 'TAXE_URB', 'Taxe à payer aux services de l''urbanisme', 'Taxe d''urbanisme', 'S', 'FCFA', 'SRUD', 1),
(2, 'TAXE_BAIL', 'Redevance Bail', 'Redevance Bail', 'S', 'FCFA', 'DOM_DP', 1),
(3, 'TAXE_MUNIC', 'Taxe municipale', 'Taxe municipale', 'S', 'FCFA', 'MD', 1);

-- --------------------------------------------------------
-- --------------------------------------------------------

--
-- Contenu de la table `taxes`
--

INSERT INTO `taxes` (`TAX_ID`, `TAX_MNT`, `TAX_DOS_ID`, `TAX_TYPT_ID`, `telepaiementState`, `TAX_ORG_CODE`) VALUES
(1, 5000.00, 5796, 1, 'NS', 'SRUD'),
(2, 45000.00, 5796, 3, 'NS', 'MAIRIE_NGOR'),
(3, 5420.00, 5799, 3, 'NS', 'MAIRIE_NGOR'),
(4, 5000.00, 5799, 1, 'NS', 'SRUD'),
(5, 1230.00, 5799, 2, 'NS', 'DOM_NAG'),
(6, 5000.00, 5800, 1, 'NS', 'SRUD'),
(7, 125000.00, 5800, 3, 'NS', 'MAIRIE_NGOR'),
(8, 34000.00, 5800, 2, 'NS', 'CONS_DOM_NA'),
(9, 5000.00, 5802, 1, 'NS', 'SDUG'),
(10, 125000.00, 5802, 3, 'NS', 'MAIRIE_GOLFSUD');

/* Mise à jour des dossiers dont le traitement est erminé mais qui ont un statut dans dos_treat_terminated à 0 */

UPDATE `dossiers` d SET d.`DOS_TREAT_TERMINATED`=1 
WHERE  d.`DOS_ID` IN 
(  select * 
    from (  select dd.dos_id 
            from `dossiers` dd 
            where dd.DOS_ETAT_CODE ="DISPONIBLE" 
            and dd.DOS_TREAT_TERMINATED =0) sd
);

UPDATE `dossiers` d SET d.`DOS_TREAT_TERMINATED`=1 
WHERE  d.`DOS_ID` IN 
(  select * 
    from (  select dd.dos_id 
            from `dossiers` dd 
            where dd.DOS_ETAT_CODE ="SIGNED_MAIRIE" 
            and dd.DOS_TREAT_TERMINATED =0) sd
);


