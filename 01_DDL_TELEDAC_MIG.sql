CREATE TABLE IF NOT EXISTS `comptes` (
  `CPT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NUM_CPT` varchar(255) DEFAULT NULL,
  `organisation_ORG_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CPT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `demat_response_object` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `currency` varchar(255) DEFAULT NULL,
  `date_transaction` varchar(255) DEFAULT NULL,
  `errorTxt` varchar(255) DEFAULT NULL,
  `montant` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `payMode` varchar(255) DEFAULT NULL,
  `refID` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `moyenstransferts` (
  `MOY_TRANSF_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE_MOY_TRANSF` varchar(255) DEFAULT NULL,
  `DES_MOY_TRANSF` varchar(255) DEFAULT NULL,
  `LIB_MOY_TRANSF` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MOY_TRANSF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `numero_dossier` (
  `ND_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `codeSite` varchar(255) DEFAULT NULL,
  `numero_current` int(11) DEFAULT NULL,
  `numero_next` int(11) DEFAULT NULL,
  PRIMARY KEY (`ND_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `paiements` (
  `PAY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PAY_DATE_PAIEMENT` datetime DEFAULT NULL,
  `ID_TRANSACTION_PAIEMENT` bigint(20) DEFAULT NULL,
  `PAY_MONTANT_GLOBAL` decimal(19,2) DEFAULT NULL,
  `PAY_STATUT_PAIEMENT` char(1) DEFAULT NULL,
  `PAY_DOS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PAY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `sites_br_recouvrements` (
  `SITE_CODE` varchar(255) NOT NULL,
  `FONCTION_CODE` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `taxes` (
  `TAX_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TAX_MNT` decimal(19,2) DEFAULT NULL,
  `TAX_DOS_ID` bigint(20) DEFAULT NULL,
  `TAX_TYPT_ID` bigint(20) DEFAULT NULL,
  `telepaiementState` varchar(255) DEFAULT NULL,
  `TAX_ORG_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TAX_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `typetaxes` (
  `TYPT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPT_CODE` varchar(255) DEFAULT NULL,
  `TYPT_DESCRIPTION` varchar(255) DEFAULT NULL,
  `TYPT_LIBELLE` varchar(255) DEFAULT NULL,
  `telepaiementState` varchar(255) DEFAULT NULL,
  `TYPT_UNITE_MONETAIRE` varchar(255) DEFAULT NULL,
  `TYPT_ORG_CODE` varchar(255) DEFAULT NULL,
  `TYPT_PROC_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`TYPT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `parametres` (
  `PRM_CODE` varchar(255) NOT NULL,
  `PRM_DESCRIPTION` varchar(255) DEFAULT NULL,
  `PRM_VALUE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PRM_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes INDEX pour la table `comptes`
--

CREATE UNIQUE INDEX `CPT_ID` ON `comptes` (`CPT_ID` ASC);

CREATE INDEX `IDX_organisation_ORG_CODE` ON `comptes` (`organisation_ORG_CODE` ASC);

--
-- Contraintes INDEX pour la table `moyenstransferts`
--

CREATE UNIQUE INDEX `MOY_TRANSF_ID` ON `moyenstransferts` (`MOY_TRANSF_ID` ASC);

--
-- Contraintes INDEX pour la table `numero_dossier`
--

CREATE UNIQUE INDEX `ND_ID` ON `numero_dossier` (`ND_ID` ASC);

--
-- Contraintes INDEX pour la table `paiements`
--

CREATE UNIQUE INDEX `PAY_ID` ON `paiements` (`PAY_ID` ASC);

CREATE INDEX `IDX_PAY_DOS_ID` ON `paiements` (`PAY_DOS_ID` ASC);

--
-- Contraintes INDEX pour la table `sites_br_recouvrements`
--

CREATE INDEX `IDX_SITE_CODE` ON `sites_br_recouvrements` (`SITE_CODE` ASC);

CREATE INDEX `IDX_FONCTION_CODE` ON `sites_br_recouvrements` (`FONCTION_CODE` ASC);

--
-- Contraintes INDEX pour la table `taxes`
--

CREATE INDEX `IDX_TAX_TYPT_ID` ON `taxes` (`TAX_TYPT_ID` ASC);

CREATE INDEX `IDX_TAX_DOS_ID` ON `taxes` (`TAX_DOS_ID` ASC);

CREATE INDEX `IDX_TAX_ORG_CODE` ON `taxes` (`TAX_ORG_CODE` ASC);

--
-- Contraintes INDEX pour la table `typetaxes`
--

CREATE INDEX `IDX_TYPT_PROC_ID` ON `typetaxes` (`TYPT_PROC_ID` ASC);

CREATE INDEX `IDX_TYPT_ORG_CODE` ON `typetaxes` (`TYPT_ORG_CODE` ASC);

--
-- Contraintes FOREIGN KEY pour la table `comptes`
--
ALTER TABLE `comptes`
  ADD CONSTRAINT `FK_organisations_ORG_CODE` FOREIGN KEY (`organisation_ORG_CODE`) REFERENCES `organisations` (`ORG_CODE`);

--
-- Contraintes ADD COLUMN 
--

alter table `dossiers` 
	add column DOS_ACTEIII TINYINT(1) ,
	add column CODE_PAIEMENT VARCHAR(255) ,
	add column telepaiementState VARCHAR(255) ,
    add column DOS_TYPE VARCHAR(32) ,
    add column DOS_COMMUNEDEPOT_CODE  VARCHAR(255) ,
    add column MOY_TRANSF_ID BIGINT(20) ;
	
	
create index IDX_DOS_COMMUNEDEPOT_CODE on `dossiers` (DOS_COMMUNEDEPOT_CODE)  ;
create index IDX_MOY_TRANSF_ID on `dossiers` (MOY_TRANSF_ID)  ;
 

alter table  `dossiers` 
add constraint FK_MOY_TRANSF_ID FOREIGN KEY (MOY_TRANSF_ID) REFERENCES moyenstransferts(MOY_TRANSF_ID);


alter table  `dossiers` 
add constraint FK_COMMUNEDEPOT_CODE FOREIGN KEY (DOS_COMMUNEDEPOT_CODE) REFERENCES sites(SIT_CODE);

  
/*

  alter table `historiques` 
  add column HIS_COMMUNE_CODE VARCHAR(255);

  alter table  `historiques` 
  add constraint FK_HIS_COMMUNE_CODE FOREIGN KEY (HIS_COMMUNE_CODE) REFERENCES organisations(ORG_CODE);
  create index IDX_HIS_COMMUNE_CODE on `historiques` (HIS_COMMUNE_CODE ASC)  ;

*/



alter table  `organisations` 
add column telepaiementState  VARCHAR(255) ,
add column ORG_STATUT INT(11) ;


alter table `procedures`  
add column PROC_CODE_PROCEDURE VARCHAR(255),
add column telepaiementState VARCHAR(255);