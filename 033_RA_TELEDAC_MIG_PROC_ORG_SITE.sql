/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


/*!40000 ALTER TABLE `procedures_organisations` DISABLE KEYS */;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_ALMADIES" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_NGOR" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_OUAKAM" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_DAKARPLATEAU" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_MEDINA" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_PLATEAU" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_GOLFSUD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_NIAYES" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_KEURMASSAR" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_MALIKA" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_THIAROYE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_DIACKSAO" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_MBAO" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_YENE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_YOFF" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_COLOBANE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_MERMOZ" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_GOREE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_FANN" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_NOTAIRE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_LIMAMOULAYE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_WAKHINANE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_YEUMNORD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_YEUMBEULSUD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_DIAMEGUENE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_THIAMER" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_THIAGARE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_PA" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_CAMBERENE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_GRANDYOFF" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_PA" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_PATTEDOIE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_GRDAK" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_DAGOUDANE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_DALIFORT" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_BISCUITERIE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_DIEUPPEUL" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_GRANDAKAR" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_HANN" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_HLM" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_THIAROYEKAO" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_GUINORD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_GUISUD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_PIKEST" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMJN" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMCS" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMTP" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMB" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMY" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="PREFET_RUFISQUE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREFECTURE_RUFISQUEEST" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMSE" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="CABMSEB" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_RUFISQUE_OUEST" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_RUFISQUE_NORD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_RUFISQUE_EST" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_BARGNY" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="DUA" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SOUSPREF_BAMBYLOR" ;

DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_PIKOUEST" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="MAIRIE_PIKNORD" ;
DELETE FROM `procedures_organisations` WHERE PO_ORG_CODE ="SP_PKG" ;


UPDATE `organisations` SET `ORG_SIT_CODE`=NULL WHERE  `ORG_CODE`='DOM_PG';
UPDATE `organisations` SET `ORG_SIT_CODE`= NULL WHERE  `ORG_CODE`='CAD_PG';

/*Fonction SDE a remettre*/
UPDATE `fonctions` SET `FCT_STATUT`=1 WHERE  `FCT_CODE`='SDE';
/*Mettre les users>Agent sap pikine/guediawaye on*/
UPDATE `utilisateurs` SET USR_STATUT=1 WHERE `USR_FCT_CODE`='ASP_GW';
/*Mettre cette fonction on*/
UPDATE `fonctions` SET `FCT_STATUT`=1 WHERE  `FCT_CODE`='ASP_GW';
/* 05-08-2016 Moussa*/
/* Remettre la transversalité de sapeur pompier rufisque et celle de guediawaye*/
UPDATE `organisations` SET `ORG_TRANSVERSALE`=1 WHERE  `ORG_CODE`='SPR';
UPDATE `organisations` SET `ORG_TRANSVERSALE`=1 WHERE  `ORG_CODE`='SPG';
/*Remettre les permissions à la fonction SDE*/
INSERT INTO `fonctions_permissions` (`FP_ID`, `FP_FCT_CODE`, `FP_PERM_ID`) VALUES (345, 'SDE', 40);
INSERT INTO `fonctions_permissions` (`FP_ID`, `FP_FCT_CODE`, `FP_PERM_ID`) VALUES (346, 'SDE', 61);
INSERT INTO `fonctions_permissions` (`FP_ID`, `FP_FCT_CODE`, `FP_PERM_ID`) VALUES (347, 'SDE', 59);
INSERT INTO `fonctions_permissions` (`FP_ID`, `FP_FCT_CODE`, `FP_PERM_ID`) VALUES (348, 'SDE', 62);




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;