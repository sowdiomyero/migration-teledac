
/* 1- changer les etats vers COMPLETED   */
update historiques
set his_etat_code = "COMPLETED", his_trans_id = 74
where  his_trans_id in (102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,119);

/* modifier l'etat de tous les dossiers qui sont entre les etats EN_ATTENTE_DUA, VALIDE_DUA et REJET_DUA les amener à COMPLETED*/

update dossiers
set dos_etatpre_code = "INSTRUCTION_INTERNE"
where  dos_etatpre_code in ("EN_ATTENTE_DUA","VALIDE_DUA","REJET_DUA");

/* tous les dossiers qui ont été avisés par les services DUA, avec des etats n'existant plus, seront remis à l'etat precedent classic : COMPLETE*/
update dossiers
set dos_etatpre_code = "COMPLETED"
where  dos_etatpre_code in ("VALIDE_DUA","REJET_DUA")
and dos_etat_code ="SIGNED_MAIRIE";

update dossiers
set dos_etat_code = "COMPLETED"
where  dos_etat_code in ("EN_ATTENTE_DUA","VALIDE_DUA","REJET_DUA");

update historiquetraitements
set ht_eta_code = "COMPLETED"
where  ht_eta_code in ("EN_ATTENTE_DUA","VALIDE_DUA","REJET_DUA");

update historiqueimputes
set hi_eta_code = "COMPLETED"
where  hi_eta_code in ("EN_ATTENTE_DUA","VALIDE_DUA","REJET_DUA");
